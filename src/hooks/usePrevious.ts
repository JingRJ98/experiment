import { ComponentState, PropsWithoutRef, useEffect, useRef } from 'react';

export default function usePrevious<T>(
  value: PropsWithoutRef<T> | ComponentState,
) {
  const ref = useRef();
  // let v
  useEffect(() => {
    ref.current = value;
    // v = value
    // console.log('修改后的 :>> ', ref.current);
  }, [value]);

  // console.log('修改前的 :>> ', ref.current);

  // return v;
  return ref.current;
}
