import '@/App.css';

import TestAntd from '@/pages/Antd';
import TestReact from '@/pages/React';
import TestComponent from '@/pages/Component';
import Tmp from './pages/Tmp';

function App() {
  return (
    <div className="container">
      {/* <TestAntd></TestAntd> */}
      {/* <TestReact></TestReact> */}
      <TestComponent></TestComponent>
      {/* <Tmp></Tmp> */}
    </div>
  );
}

export default App;
