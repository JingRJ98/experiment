import React from 'react';
import TestBadge from './components/testBadge';
import TestAntdImgCrop from './components/testAntdImgCrop';
import TestChecks from './components/testChecks';
import TestTable from './components/testTable';
import TestList from './components/testList';
import TestMenu from './components/testMenu';
import TestTabs from './components/testTabs';
import TestFilterTable from './components/testFilterTable';
import TestSelect from './components/testSelect';
import TestModalTitle from './components/testModalTitle';
import TestPopover from './components/testPopover';
import TestModal from './components/testModal';

const TestAntd: React.FC = React.memo(() => (
  <>
    {/* <TestBadge></TestBadge> */}
    {/* <TestAntdImgCrop></TestAntdImgCrop> */}
    {/* <TestChecks></TestChecks> */}
    {/* <TestTable></TestTable> */}
    {/* <TestList></TestList> */}
    {/* <TestMenu></TestMenu> */}
    {/* <TestTabs></TestTabs> */}
    {/* <TestSelect></TestSelect> */}
    {/* <TestFilterTable></TestFilterTable> */}
    {/* <TestModalTitle></TestModalTitle> */}
    {/* <TestPopover></TestPopover> */}
    <TestModal></TestModal>
  </>
));
export default TestAntd;
