import { FC, memo, useEffect, useMemo, useState } from 'react';
import { Checkbox } from 'antd';
const { Group } = Checkbox;

const Index: FC = memo(() => {
  const [checkedlist, setcheckedlist] = useState<string[]>([]);
  const [indeterminate, setindeterminate] = useState(false);
  const [checkAll, setcheckAll] = useState(false);

  const obj = {
    a: {},
    b: {},
    c: {},
    d: {},
    e: { num: 1 },
    // e: { num: 0 },
  };

  const isDisabled = useMemo(() => {
    return obj['e'].num === 0;
  }, []);

  const onChange = (list: any) => {
    setcheckedlist(list);
  };

  const oncheckAll = (e: any) => {
    setcheckAll(e.target.checked);
    const checks = isDisabled
      ? Object.keys(obj).filter((item) => item !== 'e')
      : Object.keys(obj);
    setcheckedlist(e.target.checked ? checks : []);
  };

  useEffect(() => {
    const all = isDisabled
      ? Object.keys(obj).filter((item) => item !== 'e')
      : Object.keys(obj);
    setindeterminate(!!checkedlist.length && checkedlist.length < all.length);
    setcheckAll(checkedlist.length === all.length);
  }, [checkedlist]);

  console.log(Object.values(obj.e));

  return (
    <div>
      <div>
        <Checkbox
          indeterminate={indeterminate}
          checked={checkAll}
          onChange={oncheckAll}
        >
          全选
        </Checkbox>
      </div>
      <Group onChange={onChange} value={checkedlist}>
        {Object.keys(obj).map((k) => (
          <Checkbox value={k} disabled={isDisabled && k === 'e'} key={k}>
            {k}
          </Checkbox>
        ))}
      </Group>
    </div>
  );
});

export default Index;
