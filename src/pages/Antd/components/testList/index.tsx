import React, {
  useEffect,
  useState,
  FC,
  memo,
  useCallback,
  useRef,
} from 'react';
import { Divider, Skeleton } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';

import styles from './style.module.scss';

interface DataType {
  gender: string;
  name: {
    title: string;
    first: string;
    last: string;
  };
  email: string;
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  };
  nat: string;
}

const fun = () => {
  const res = [];
  for (let i = 0; i < 400; i++) {
    res.push(`Hello world ${i}`);
  }
  return res;
};

const TestList: FC = memo(() => {
  const [data, setData] = useState<any[]>([]);
  const nextIndex = useRef<number>(0);

  const dataSource = fun();
  const loadMoreData = useCallback(() => {
    // fetch(
    //   'https://randomuser.me/api/?results=10&inc=name,gender,email,nat,picture&noinfo',
    // )
    //   .then((res) => res.json())
    //   .then((body) => {
    //     setData([...data, ...body.results]);
    //     setLoading(false);
    //   })
    //   .catch(() => {
    //     setLoading(false);
    //   });
    setData((data) => [
      ...data,
      ...dataSource.slice(nextIndex.current, nextIndex.current + 20),
    ]);
    nextIndex.current = nextIndex.current + 20;
  }, []);

  useEffect(() => {
    loadMoreData();
  }, []);
  return (
    <div
      id="scrollableDiv"
      style={{
        height: 400,
        overflow: 'auto',
        padding: '0 16px',
        border: '1px solid rgba(140, 140, 140, 0.35)',
      }}
      className={styles.container}
    >
      <InfiniteScroll
        dataLength={data.length}
        next={loadMoreData}
        hasMore={data.length < 40}
        loader={null}
        // endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
        scrollableTarget="scrollableDiv"
      >
        {data.map((v) => {
          return <div>{v}</div>;
        })}
      </InfiniteScroll>
    </div>
  );
});

export default TestList;
