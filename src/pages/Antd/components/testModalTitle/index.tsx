import React, { FC, memo, useCallback, useEffect, useMemo, useState } from 'react';
import cx from 'classnames'

import { Button, Divider, Modal, Select, Radio, Tooltip, Typography } from 'antd';
const { Text } = Typography

import styles from './style.module.scss';

interface IndexProps { }

enum AIAnnotationType {
  CT = 'ct',
  MR = 'mr'
}



const TabTitle = ({ setAnnoType }: { setAnnoType: (type: AIAnnotationType) => void }) => {

  const [active, setActive] = useState<AIAnnotationType>(AIAnnotationType.CT)

  useEffect(() => {
    setAnnoType(active)
  }, [active])

  return (
    <div className={styles.tabTitle}>
      <div className={cx(styles.tabTitleItem, { [styles.active]: active === AIAnnotationType.CT })} onClick={() => setActive(AIAnnotationType.CT)}>CT</div>
      <div className={cx(styles.tabTitleItem, { [styles.active]: active === AIAnnotationType.MR })} onClick={() => setActive(AIAnnotationType.MR)}>MR</div>
    </div>
  )
}

const List = ({ data }: { data: any[] }) => {
  const [extend, setExtend] = useState<boolean[]>(new Array(data.length).fill(false))
  const [selectThirdItem, setSelectThirdItem] = useState('')

  const onClick = useCallback((v, idx) => {
    console.log('v :>> ', v);
    setExtend(arr => {
      const newArr = [...arr]
      newArr[idx] = !arr[idx]
      return newArr
    })
  }, [])

  const onChange = useCallback((e) => {
    console.log('v :>> ', e.target.value);
    setSelectThirdItem(e.target.value)
  }, [])

  return (
    <div className={styles.list}>
      {
        data.map((v, i) => (
          <>
            <div className={styles.item} onClick={() => onClick(v, i)}>
              {v.value}
            </div>
            {
              extend[i] && <div className={styles.children}>
                <Radio.Group onChange={onChange} value={selectThirdItem}>
                  {
                    v.children.map((vv, ii) => (
                      <Radio value={`${i}-${ii}`}>
                        <Text ellipsis={{tooltip: `${vv}`}} className={styles.xxxx}>
                            {vv}
                        </Text>
                      </Radio>
                    ))
                  }
                </Radio.Group>
              </div>
            }
          </>
        ))
      }
    </div>
  )
}

const Content = ({ annoType }: { annoType: AIAnnotationType }) => {
  const listData: any[] = []

  for (let i = 0; i < 80; i++) {
    const rv = `${i}-${Math.random().toString(36).slice(2)}`
    const children = new Array(20).fill(0).map((_, ii) => (
      `${i}-${ii}-${'dadasdasfdre3qfre3qfew'}`
    ))
    listData.push({
      id: i,
      value: rv,
      children
    })

  }

  return (
    <div className={styles.content}>
      {annoType}
      <Select
        showSearch
      >

      </Select>

      <List
        data={listData}
      />
    </div>
  )
}

/**
 * Modal的title设置为tab
 */
const TestModalTitle: FC<IndexProps> = memo((props) => {
  const { } = props;
  const [isModalOpen, setIsModalOpen] = useState(true);
  const [annoType, setAnnoType] = useState<AIAnnotationType>(AIAnnotationType.CT)

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // const modalTitle = useMemo(() => {

  //   return (
  //     <div>
  //       <div>CT</div>
  //       <div>MR</div>
  //     </div>
  //   )
  // }, [])

  return (
    <div>
      <Button type="primary" onClick={showModal}>
        Open Modal
      </Button>
      <Modal
        // title={modalTitle}
        title={<TabTitle setAnnoType={setAnnoType} />}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div>
          <Content annoType={annoType} />
        </div>
      </Modal>
    </div>
  )
});
export default TestModalTitle;