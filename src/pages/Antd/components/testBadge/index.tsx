import React, { FC, memo } from 'react';
import { Badge } from 'antd';

import styles from './style.module.scss';

const TestBadge: FC = memo(() => {
  const c = 100;
  return (
    <Badge count={c ? `+${c}` : 0} overflowCount={999} color={'red'}>
      <div className={styles.div}></div>
    </Badge>
  );
});

export default TestBadge;
