import React, { FC, memo } from 'react';
import { Popover } from 'antd';

import styles from './index.module.scss';

interface TestPopoverProps {}

const Comp = (props: {name: string}) => {
  const {name} = props
  return (
    <div
      {
        ...props
      }
    >Hover我试试{name}</div>
  )
}

const TestPopover: FC<TestPopoverProps> = memo((props) => {
  const content = <div>Hello world</div>;
  return (
    <Popover
      overlayClassName={styles.linkCardPopover}
      showArrow={false}
      align={{ offset: [0,0] }}
      placement="bottom"
      content={content}
    >
      {/* <div className={styles.inner}>Hover我试试</div> */}
      <Comp name={'jrj'}/>
    </Popover>
  );
});

export default TestPopover;
