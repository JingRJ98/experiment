import { Select, Tooltip } from 'antd';
import React, { FC, memo, useCallback, useEffect, useMemo } from 'react';

interface TestSelectProps {}

const TestSelect: FC<TestSelectProps> = memo((props) => {
  const data = useMemo(() => {
    const res = [];
    for (let i = 0; i < 100; i++) {
      res.push({
        label: Math.random(),
        value: i,
      });
    }
    return res;
  }, []);

  const handleSelect = useCallback(
    (input, option) => {
      const target = data.find(({ value }) => value === option.value);
      return !!target?.label.toString().includes(`${input}`);
    },
    [data],
  );

  const onChange = () => {
    console.log('change事件');
  };

  const onFocus = () => {
    console.log('获得焦点');
  };

  const onBlur = () => {
    console.log('失去焦点');
  };

  const onSearch = (e) => {
    e.stopPropagation();
  };

  const onkeydown = (e) => {
    console.log('有按键按下', e.keyCode);
  };
  useEffect(() => {
    document.addEventListener('keydown', onkeydown);
  }, []);

  return (
    <div style={{ width: '200px' }}>
      <Select
        showSearch
        onChange={onChange}
        filterOption={handleSelect}
        placeholder="请选择你需要的数字"
        onFocus={onFocus}
        onBlur={onBlur}
        onSearch={onSearch}
      >
        {data.map((i) => (
          <Select.Option key={i.value} value={i.value}>
            <Tooltip title={i.label}>{i.label}</Tooltip>
          </Select.Option>
        ))}
      </Select>
    </div>
  );
});
export default TestSelect;
