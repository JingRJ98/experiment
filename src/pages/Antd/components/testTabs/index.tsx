import React, { FC, memo, useCallback, useMemo, useRef, useState } from 'react';
import { Button, Tabs } from 'antd';

import styles from './style.module.scss';
import Content from './components/Content';

interface DataTypeTabsProps {}

const { TabPane } = Tabs;
const DataTypeTabs: FC<DataTypeTabsProps> = memo((props) => {
  const {} = props;

  const defaultPanes = new Array(2).fill(null).map((_, index) => {
    const id = String(index + 1);
    return {
      label: `Tab ${id}`,
      children: `Content of Tab Pane ${index + 1}`,
      key: id,
    };
  });

  const [activeKey, setActiveKey] = useState(defaultPanes[0].key);
  const [items, setItems] = useState(defaultPanes);
  const newTabIndex = useRef(0);

  const onChange = (key: string) => {
    setActiveKey(key);
  };

  const add = useCallback(() => {
    const newActiveKey = `newTab${newTabIndex.current++}`;
    setItems([
      ...items,
      {
        label: `New Tab ${newTabIndex.current}`,
        children: 'New Tab Pane',
        key: newActiveKey,
      },
    ]);
    setActiveKey(newActiveKey);
  }, [newTabIndex.current]);

  const remove = (targetKey: string) => {
    const targetIndex = items.findIndex((pane) => pane.key === targetKey);
    const newPanes = items.filter((pane) => pane.key !== targetKey);
    if (newPanes.length && targetKey === activeKey) {
      const { key } =
        newPanes[
          targetIndex === newPanes.length ? targetIndex - 1 : targetIndex
        ];
      setActiveKey(key);
    }
    setItems(newPanes);
  };

  const onEdit = (
    targetKey: React.MouseEvent | React.KeyboardEvent | string,
    action: 'add' | 'remove',
  ) => {
    if (action === 'remove') {
      remove(targetKey as string);
    }
  };

  const addTab = useMemo(
    () => <Button onClick={add}>新增数据类型</Button>,
    [add],
  );

  return (
    <div className={styles.container}>
      <Tabs
        hideAdd
        onChange={onChange}
        activeKey={activeKey!}
        type="editable-card"
        onEdit={onEdit}
        tabBarExtraContent={addTab}
      >
        {items.map(({ label, children, key }, i) => (
          <TabPane tab={label} key={key}>
            <Content />
          </TabPane>
        ))}
      </Tabs>
    </div>
  );
});
export default DataTypeTabs;
