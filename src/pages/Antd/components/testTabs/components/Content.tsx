import React, { FC, memo } from 'react';

import styles from './style.module.scss';

interface ContentProps {}

const Content: FC<ContentProps> = memo((props) => {
  const {} = props;

  return <div className={styles.container}></div>;
});
export default Content;
