import React, { FC, memo } from 'react';

import styles from './style.module.scss';

interface IDesc {
  type: 'desc' | 'image';
  content?: React.ReactNode[];
  src?: string;
}

interface IPage {
  type: 'paragraph';
  title: React.ReactNode;
  body: IDesc[];
}

interface ContentProps {
  dataSource: {
    type: 'page';
    body: IPage[];
  };
}

const Content: FC<ContentProps> = memo((props) => {
  const { dataSource } = props;

  return (
    <div>
      <div className={styles.test}></div>
    </div>
  );
});
export default Content;
