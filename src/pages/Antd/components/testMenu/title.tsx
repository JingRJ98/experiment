import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
type MenuItem = Required<MenuProps>['items'][number];

export const TITLE = [
  {
    key: 'dataInfo',
    label: '数据相关',
    icon: <AppstoreOutlined />,
    children: [
      {
        title: 'a',
        key: 'dataInfo-a',
        label: '数据变量类型',
      },
      {
        title: 'b',
        key: 'dataInfo-b',
        label: '哑变量',
      },
    ],
  },
  {
    key: 'dataPartitioning',
    label: '数据集划分',
    icon: <AppstoreOutlined />,
    children: [
      // 预留训练集,验证集和测试集
      {
        title: 'b',
        key: 'dataPartitioning-b',
        label: '随机种子',
      },
    ],
  },
  {
    key: 'dicomOmicsFeature',
    label: '影像组学特征提取',
    icon: <AppstoreOutlined />,
    children: [
      {
        title: 'a',
        key: 'dicomOmicsFeature-a',
        label: '数据预处理',
      },
      {
        title: 'b',
        key: 'dicomOmicsFeature-b',
        label: '特征提取',
      },
    ],
  },
  {
    key: 'featureSelection',
    label: '特征筛选',
    icon: <AppstoreOutlined />,
    children: [
      // 预留特征数据标准化
      {
        title: 'b',
        key: 'featureSelection-b',
        label: '特征预选',
      },
      {
        title: 'c',
        key: 'featureSelection-c',
        label: '单变量筛选',
      },
      {
        title: 'd',
        key: 'featureSelection-d',
        label: '基于模型的特征筛选',
      },
    ],
  },
  {
    key: 'mlModel',
    label: '机器学习网络模型',
    icon: <AppstoreOutlined />,
    children: [
      {
        title: 'a',
        key: 'mlModel-a',
        label: 'Logistic Regression(LR)',
      },
      {
        title: 'b',
        key: 'mlModel-b',
        label: 'SupportVectorMachine(SVM)',
      },
      {
        title: 'c',
        key: 'mlModel-c',
        label: 'RadomForest(RF)',
      },
      {
        title: 'd',
        key: 'mlModel-d',
        label: 'Stochastic Gradient Descent(SGD)',
      },
      {
        title: 'e',
        key: 'mlModel-e',
        label: 'Adaboost',
      },
      {
        title: 'f',
        key: 'mlModel-f',
        label: 'XGBoost',
      },
      {
        title: 'g',
        key: 'mlModel-g',
        label: 'PassiveAggressive(PA)',
      },
      {
        title: 'h',
        key: 'mlModel-h',
        label: 'Perceptron',
      },
      {
        title: 'i',
        key: 'mlModel-i',
        label: 'LinearSVC',
      },
      {
        title: 'j',
        key: 'mlModel-j',
        label: 'GaussianNB',
      },
      {
        title: 'k',
        key: 'mlModel-k',
        label: 'DecisionTree(DT)',
      },
      {
        title: 'l',
        key: 'mlModel-l',
        label: 'KNearNeighbors(KNN)',
      },
    ],
  },
  {
    key: 'dataEnhancement',
    label: '数据增强',
    icon: <AppstoreOutlined />,
    children: [
      // 预留特征数据标准化
      {
        title: 'a',
        key: 'dataEnhancement-a',
        label: '空间增强',
      },
      {
        title: 'b',
        key: 'dataEnhancement-b',
        label: '像素增强',
      },
    ],
  },
  {
    key: 'dlModel',
    label: '深度学习网络模型',
    icon: <AppstoreOutlined />,
    children: [
      {
        title: 'a',
        key: 'dlModel-a',
        label: 'ResNet-分类网络模型',
      },
      {
        title: 'b',
        key: 'dlModel-b',
        label: 'DenseNet-分类网络模型',
      },
      {
        title: 'c',
        key: 'dlModel-c',
        label: 'SENet-分类网络模型',
      },
      {
        title: 'd',
        key: 'dlModel-d',
        label: 'UNET-分割网络模型',
      },
      {
        title: 'e',
        key: 'dlModel-e',
        label: 'ResNet-分割网络模型',
      },
      {
        title: 'f',
        key: 'dlModel-f',
        label: 'UNETR-分割网络模型',
      },
    ],
  },
  {
    key: 'dlAdjustParam',
    label: '深度学习网络调参',
    icon: <AppstoreOutlined />,
    children: [
      // 预留基础参数
      {
        title: 'b',
        key: 'dlAdjustParam-b',
        label: '损失函数',
      },
      {
        title: 'c',
        key: 'dlAdjustParam-c',
        label: '优化器',
      },
    ],
  },
  {
    key: 'survivalAnalysisModel',
    label: '生存分析网络模型',
    icon: <AppstoreOutlined />,
    children: [
      {
        title: 'a',
        key: 'survivalAnalysisModel-a',
        label: '多因素cox回归',
      },
    ],
  },
  {
    key: 'modelResult',
    label: '常用结果图示',
    icon: <AppstoreOutlined />,
    children: [
      {
        title: 'a',
        key: 'modelResult-a',
        label: '混淆矩阵',
      },
      {
        title: 'b',
        key: 'modelResult-b',
        label: 'ROC',
      },
      {
        title: 'c',
        key: 'modelResult-c',
        label: 'AUC',
      },
      {
        title: 'd',
        key: 'modelResult-d',
        label: '森林图',
      },
      {
        title: 'e',
        key: 'modelResult-e',
        label: '诺莫图',
      },
      {
        title: 'f',
        key: 'modelResult-f',
        label: '校准曲线',
      },
      {
        title: 'g',
        key: 'modelResult-g',
        label: 'PR曲线',
      },
      {
        title: 'h',
        key: 'modelResult-h',
        label: '决策曲线',
      },
      {
        title: 'i',
        key: 'modelResult-i',
        label: '特征权重图',
      },
    ],
  },
];
