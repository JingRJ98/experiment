import React, { FC, memo, useState } from 'react';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import { TITLE } from './title';
import Content from './Content';

const rootSubmenuKeys = TITLE.map(({ key }) => key);
const TestMenu: FC = memo(() => {
  const [openKeys, setOpenKeys] = useState(['dataInfo']);

  const onOpenChange: MenuProps['onOpenChange'] = (keys) => {
    // keys 菜单打开添加,关闭移除
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  let a: any;
  return (
    <div>
      <h2>Menu</h2>
      <Content dataSource={a}></Content>
      <Menu
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        style={{ width: 256 }}
        items={TITLE}
      />
    </div>
  );
});
export default TestMenu;
