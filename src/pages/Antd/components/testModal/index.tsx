import { Modal, Checkbox } from 'antd';
import cls from 'classnames';

import styles from './index.module.scss';

const RtcModal = () => {
  const store = {
    visible: true,
    title: '',
    // content: '开启共享屏幕后，摄像头将被关闭',
    content: (
      <div className={styles.testContent}>开启共享屏幕后，摄像头将被关闭</div>
    ),
    onOk: () => {
      console.log('onOk');
    },
    onCancel: () => {
      console.log('onCancel');
    },
    // onlyOkBtn: true,
    onlyOkBtn: false,
    // hasNotShowAgain: false,
    hasNotShowAgain: true,
    okText: '开启视频',
    cancelText: '取消',
  };

  const {
    visible,
    title,
    content,
    onOk,
    onCancel,
    onlyOkBtn,
    hasNotShowAgain,
    okText,
    cancelText,
  } = store;

  const closeIcon = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="28"
      height="28"
      viewBox="0 0 28 28"
      fill="none"
    >
      <rect opacity="0.5" width="28" height="28" rx="6" fill="#3A3A3D" />
      <path
        d="M7.4668 20.5354L20.5335 7.46875"
        stroke="white"
        stroke-opacity="0.9"
        stroke-width="2.66667"
        stroke-linecap="round"
      />
      <path
        d="M20.5332 20.5354L7.46654 7.46875"
        stroke="white"
        stroke-opacity="0.9"
        stroke-width="2.66667"
        stroke-linecap="round"
      />
    </svg>
  );

  return (
    <Modal
      visible={visible}
      footer={null}
      width={316}
      className={styles.rtcModal}
      centered
      closeIcon={closeIcon}
    >
      <div>
        <div>{title}</div>
        <div className={styles.content}>{content}</div>
        <div
          className={cls(styles.btnsWrapper, {
            [styles.twoBtns]: !onlyOkBtn,
          })}
        >
          {!onlyOkBtn && (
            <div className={cls(styles.btn, styles.cancelBtn)} onClick={() => onCancel()}>
              {cancelText}
            </div>
          )}
          <div className={cls(styles.btn, styles.okBtn)} onClick={() => onOk()}>{okText}</div>
        </div>
        {hasNotShowAgain && (
          <div className={styles.notShowAgain}>
            <Checkbox className={styles.checkbox} onChange={() => {}}>不再提示</Checkbox>
          </div>
        )}
      </div>
    </Modal>
  );
};

export default RtcModal;
