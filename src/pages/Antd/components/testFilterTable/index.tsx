import React, { FC, memo } from 'react';
import type { ColumnsType } from 'antd/es/table';

import FilterTable from './components/FilterTable';

import styles from './style.module.scss';

interface TestFilterTableProps {}

interface DataType {
  name: string;
  age: number;
  address: string;
}

const TestFilterTable: FC<TestFilterTableProps> = memo((props) => {
  const {} = props;

  const getDataSource = () => {
    const data: DataType[] = [];
    const getName = () => String.fromCharCode(((Math.random() * 26) >> 0) + 97);
    for (let i = 0; i < 400; i++) {
      const name = `${getName()}${getName()}${getName()}${getName()}`;
      data.push({
        name,
        age: (Math.random() * 100) >> 0,
        address: name,
      });
    }
    return data;
  };

  const columns: ColumnsType<DataType> = [
    { title: 'Name', dataIndex: 'name' },
    { title: 'Age', dataIndex: 'age' },
    { title: 'Address', dataIndex: 'address' },
  ];

  return (
    <div className={styles.container}>
      <div className={styles.header}></div>
      <FilterTable columns={columns} data={getDataSource()} />
    </div>
  );
});
export default TestFilterTable;
