import React, { FC, memo } from 'react';
// import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

// import styles from './style.module.scss';

// interface FilterTableProps<T> {
//   columns: ColumnsType<T>;
//   data: T[];
// }

// const FilterTable: FC<FilterTableProps<any>> = memo((props) => {
//   const {columns, data} = props;

//   return (
//     <div className={styles.container}>
//       <Table
//         columns={columns}
//         dataSource={data}
//       />
//     </div>
//   )
// });
// export default FilterTable;

// 组件的props
interface IProps<T extends unknown = any> {
  a: T;
  f: ColumnsType<T>;
}

const App: FC<IProps> = (props) => {
  return <></>;
};

type A = {
  a: string;
  b: number;
};

type B = {
  a: number;
  b: string;
};
