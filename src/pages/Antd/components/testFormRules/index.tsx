import { Form, Input } from 'antd';
import React, { FC } from 'react';

export const TestFormRules: FC = () => {
  const [form] = Form.useForm();
  const project = {};
  const layout = {};
  const onValuesChange = () => {};
  return (
    <div>
      <Form
        form={form}
        {...layout}
        initialValues={project}
        onValuesChange={onValuesChange}
      >
        <Form.Item
          name="p_name"
          label="项目名称"
          rules={[
            { required: true },
            {
              pattern: /^(?!\s)(?!.*\s$)/,
              message: '项目名称首尾不能包含空格',
            },
          ]}
          colon={false}
        >
          <Input.TextArea
            showCount={true}
            autoSize={{ minRows: 4, maxRows: 8 }}
            maxLength={100}
            placeholder="请输入项目名称"
          />
        </Form.Item>
      </Form>
    </div>
  );
};
