import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import React, { FC, memo } from 'react';

interface DataType {
  key: string;
  name: string;
  age: number;
  tel: string;
  phone: number;
  address: string;
}

// In the fifth row, other columns are merged into first column
// by setting it's colSpan to be 0
// const sharedOnCell = (_: DataType, index: number) => {
//   if (index === 4) {
//     return { colSpan: 0 };
//   }

//   return {};
// };

const columns: ColumnsType<DataType> = [
  {
    title: 'Name',
    dataIndex: 'name',
    render: (text) => <a>{text}</a>,
    onCell: (_, index) => {
      // 当前列
      // 第一行向下渲染三行
      if (index === 0) {
        return { rowSpan: 3 };
      }
      // 第二三行不渲染
      if ((index ?? 0) < 3) {
        return { rowSpan: 0 };
      }
      return {};
    },
  },
  {
    title: 'Age',
    dataIndex: 'age',
    // onCell: sharedOnCell,
  },
  {
    title: 'Home phone',
    colSpan: 2,
    dataIndex: 'tel',
    // onCell: (_, index) => {
    //   if (index === 2) {
    //     return { rowSpan: 2 };
    //   }
    //   // These two are merged into above cell
    //   if (index === 3) {
    //     return { rowSpan: 0 };
    //   }
    //   if (index === 4) {
    //     return { colSpan: 0 };
    //   }

    //   return {};
    // },
  },
  {
    title: 'Phone',
    colSpan: 0,
    dataIndex: 'phone',
    // onCell: sharedOnCell,
  },
  {
    title: 'Address',
    dataIndex: 'address',
    // onCell: sharedOnCell,
  },
];

const data: DataType[] = [
  {
    key: '1',
    name: 'a',
    age: 32,
    tel: '0571-22098909',
    phone: 18889898989,
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    name: 'b',
    tel: '0571-22098333',
    phone: 18889898888,
    age: 42,
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    name: 'c',
    age: 32,
    tel: '0575-22098909',
    phone: 18900010002,
    address: 'Sidney No. 1 Lake Park',
  },
  {
    key: '4',
    name: 'd',
    age: 18,
    tel: '0575-22098909',
    phone: 18900010002,
    address: 'London No. 2 Lake Park',
  },
  {
    key: '5',
    name: 'e',
    age: 18,
    tel: '0575-22098909',
    phone: 18900010002,
    address: 'Dublin No. 2 Lake Park',
  },
];

const App: FC = memo(() => (
  // 关于合并部分列, 先构造出每一行单独的数据, 即需要合并的行中 有部分列的值是一样的
  <Table columns={columns} dataSource={data} bordered />
));

export default App;


import React, { useEffect, useState } from 'react'
import { Table} from 'antd'

export default function Index() {
  const [ tableData, setTableData ] = useState<any>([])

  const columns = [
    {
        title: '分类',
        dataIndex: 'category',
        onCell: (row:any) => ({ rowSpan: row.rowSpan || 0 })
    },
    {
        title: '名称',
        dataIndex: 'name',
    },
    {
        title: '评价',
        dataIndex: 'desc',
    },
  ];

  useEffect(() => {
    // 模拟生命周期接口获取数据、处理数据
    let data = [
      {
        "category":"水果",
        "name": "桃",
        "desc": "桃子好吃"
      },{
        "category":"水果",
        "name": "梨",
        "desc": "梨子真好吃"
      },{
        "category":"蔬菜",
        "name": "茄子",
        "desc": "茄子茄子"
      },{
        "category":"家禽",
        "name": "牛肉",
        "desc": "吃不起"
      },{
        "category":"家禽",
        "name": "羊肉",
        "desc": "羊肉好羊肉"
      },{
        "category":"家禽",
        "name": "猪肉",
        "desc": "good"
      }
    ]
    let res = handleData(data, 'category')
    setTableData(res)
  }, [])

  // 处理数据rowSpan函数
  const handleData = (array, key) => {
    if(array.length === 0) return
    let arr = [...array]
    // 1、startItem(默认rowSpan = 1)记录开始计数的对象
    let startItem:any = arr[0]
    startItem.rowSpan = 1
    // 2、遍历数组，取下一项进行比较，当name相同则startItem的rowSpan+1, 否则设置新的startItem为下一项
    arr.forEach((item:any, index) => {
      let nextItem:any = arr[index+1] || {}
      if(item[key] === nextItem[key]){
        startItem.rowSpan++
      }else{
        startItem = nextItem
        startItem.rowSpan = 1
      }
    })
    return arr
  }

  return (
    <Table
      bordered
      rowKey="name"
      columns={columns}
      dataSource={tableData}
      pagination={false}
    />
  )
}
