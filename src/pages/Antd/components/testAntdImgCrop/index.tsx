import React, { FC, memo } from 'react';
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';

const TestAntdImgCrop: FC = memo(() => {
  return (
    <div>
      <div>TestAntdImgCrop</div>
      <ImgCrop>
        <Upload listType="picture-card">+Add Img</Upload>
      </ImgCrop>
    </div>
  );
});
export default TestAntdImgCrop;
