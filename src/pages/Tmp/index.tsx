import React, { FC, memo, useEffect, useMemo, useRef, useState } from 'react';

import styles from './style.module.scss';

/**
 * @param {Object} props
 * @param {string} props.name
 * @param {Option[]} props.options
 * @param {Function} props.onChange
 * @return {JSX.Element}
 */
const Tmp = () => {
  const [s, setS] = useState(0)
  const ref = useRef(null)

  console.log('ref.current :>> ', ref.current);

  useEffect(() => {
    console.log('ref.current in useEffect :>> ', ref.current);
  }, [])

  return (
    <div className={styles.container}>
      <div ref={ref}></div>
    </div>
  )
}

export default Tmp