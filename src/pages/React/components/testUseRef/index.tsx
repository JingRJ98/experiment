import React, { FC, memo, useRef, useState } from 'react';

import styles from './style.module.scss';

interface IndexProps { }

function useLatest<T>(value: T) {
  const ref = useRef(value);
  ref.current = value;

  return ref;
}



const Index: FC<IndexProps> = memo((props) => {
  const { } = props;

  const [s, setS] = useState(0)

  const latestS = useLatest(s)


console.log(latestS.current);



  return (
    <>
      {/* <div>{latestS.current}</div> */}
      <div onClick={() => setS(100)}>点我</div>
    </>
  )
});
export default Index;