import React, { FC, memo, useCallback, useState, useTransition } from 'react';

const TestUseTransition: FC = memo(() => {
  const [num, setNum] = useState(0);
  const [isPending, startTransition] = useTransition();

  // 点击后屏幕中会闪现红色的666
  // 最终显示为黑色的777
  const onClick = useCallback(() => {
    setNum(666);
    startTransition(() => {
      setNum(777);
    });
  }, []);
  return (
    <div onClick={onClick}>
      点我测试useTransition
      <p style={{ color: isPending ? 'red' : 'black' }}>现在的 num{num}</p>
    </div>
  );
});
export default TestUseTransition;
