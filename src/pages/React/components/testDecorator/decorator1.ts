function ClassDe(...args: any) {
  console.log('类装饰器', args);
}

function ConsPramsDe(...args: any) {
  console.log('类构造函数参数装饰器', args);
}

function PropDe(...args: any) {
  console.log('属性装饰器', args);
}

function MethodDe(...args: any) {
  console.log('方法装饰器', args);
}
function MethodDe2(...args: any) {
  console.log('方法装饰器2', args);
}
function MethodParams(...args: any) {
  console.log('方法参数参数装饰器', args);
}
function MethodParams2(...args: any) {
  console.log('方法参数参数装饰器', args);
}

@ClassDe
export class Person {
  @PropDe
  private age!: number;

  constructor(@ConsPramsDe private name: string) {}

  @MethodDe
  say(@MethodParams name: string) {}

  @MethodDe2
  say2(@MethodParams2 name2: string) {}
}
