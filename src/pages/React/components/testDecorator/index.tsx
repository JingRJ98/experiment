import React, { FC, memo } from 'react';
import Person from './decorator';
import { Person as Person1 } from './decorator1';

interface IndexProps {}
// const p = new Person()
// p.say("zh", "llm")

const p1 = new Person1('jrj');

const Index: FC<IndexProps> = memo((props) => {
  return <div>测试装饰器</div>;
});
export default Index;
