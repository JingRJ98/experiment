function MethodInterceptor(
  ClassPrototype: any,
  methodName: any,
  methodDesc: PropertyDescriptor,
) {
  // 保存以前的函数体
  let origin = methodDesc.value;
  console.log('origin:', origin);

  // 修改函数体
  methodDesc.value = function (...args: any[]) {
    console.log('this:', this); // Person
    // 迭代所有参数
    args = args.map((arg) => arg + '增加相同内容');
    console.log('args', args); // [ 'zh增加相同内容', 'llm增加相同内容' ]

    // 调用以前的函数
    origin.apply(this, args);
  };
}

export default class Person {
  constructor() {}

  @MethodInterceptor
  say(p1: string, p2: string) {
    console.log('say.....p1, p2', p1, p2);
  }
}

// say.....p1, p2 zh增加相同内容 llm增加相同内容
