import React, { FC, memo } from 'react';
import Parent from './parent';

import styles from './style.module.scss';

interface TestFoWardRefProps {}

const TestFoWardRef: FC<TestFoWardRefProps> = memo((props) => {
  return (
    <div>
      TestFoWardRef
      <Parent />
    </div>
  );
});
export default TestFoWardRef;
