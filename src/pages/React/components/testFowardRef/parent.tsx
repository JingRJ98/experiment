import React, { FC, memo, useEffect, useRef } from 'react';
import Child, { ChildRef } from './child';

interface ParentProps {}

const Parent: FC<ParentProps> = memo(() => {
  // 父组件只能拿到子组件的一个方法: testFocus
  const childRef = useRef<ChildRef>(null);

  useEffect(() => {
    if (childRef.current) {
      console.log('childRef.current', childRef.current);
      childRef.current?.testFocus();
    }
  }, []);
  return (
    <div>
      Parent
      <Child ref={childRef} name="jrj" age={18} />
    </div>
  );
});
export default Parent;
