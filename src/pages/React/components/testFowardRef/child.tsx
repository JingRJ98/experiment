import { memo, useImperativeHandle, useRef, forwardRef, ForwardRefRenderFunction } from 'react';

interface ChildProps {
  name: string;
  age: number;
}

export type ChildRef = {
  /** 此时父组件应只有这一个属性 */
  testFocus: () => void;
};

// 注意ref的类型写在前面, props的类型写在后面
const Child: ForwardRefRenderFunction<ChildRef, ChildProps> = (
  props,
  ref,
) => {
  console.log('props :>> ', props);
  console.log('ref', ref);

  // 定义一个自己的ref
  const selfRef = useRef<HTMLInputElement>(null);

  useImperativeHandle(
    // 父组件传入的ref
    ref,
    () => {
      // 向外暴露的方法
      return {
        testFocus() {
          selfRef.current?.focus();
        },
      };
    },
    [],
  );
  return (
    <div>
      Child
      <input placeholder="请测试fowardRef" ref={selfRef} />
    </div>
  );
};

export default memo(forwardRef(Child));
