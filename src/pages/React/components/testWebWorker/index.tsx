import { Button, Card } from 'antd';
import React, { FC, memo, useEffect, useRef, useState } from 'react';
import fibWorker from './fib.worker';

const Count: FC = memo(() => {
  const [n, setN] = useState<number>(20);
  const [fib, setFib] = useState(1);
  const computeFib = useRef<any>(null);

  // const handle= (n) => {
  //   if(n < 2)return 1
  //   return handle(n - 1) + handle(n - 2)
  // }

  useEffect(() => {
    // const f = handle(n)
    // setFib(f)
    computeFib.current = new Worker('./fib.worker.ts');
    // computeFib.current = new fibWorker()
    computeFib.current?.addEventListener('message', (e) => {
      console.log('e.data :>> ', e.data);
    });
    return () => {
      computeFib.current?.terminate();
    };
  }, []);

  useEffect(() => {
    computeFib.current?.postMessage(n);
  }, [n]);

  return (
    <div>
      <p>利用web worker计算第n个数字的斐波那契数列</p>
      <p style={{ fontSize: '16px' }}>
        只是为了检验web worker的多线程计算功能, 递归计算fib不是一个好方法
      </p>
      <Card>
        <p>
          第{n}个斐波那契数是{fib}
        </p>
        <Button onClick={() => setN((n) => n + 1)}>+1</Button>
      </Card>
    </div>
  );
});
export default Count;
