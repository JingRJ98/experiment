self.onmessage = (e) => {
  const { n } = e.data;

  console.log('n :>> ', n);
  const handle = (n) => {
    if (n < 2) return 1;
    return handle(n - 1) + handle(n - 2);
  };

  // self.postMessage(handle(n))
  self.postMessage('我收到了');
};

export default self;
