import React, { FC, memo } from 'react';
import { Routes, Route, Navigate, BrowserRouter } from 'react-router-dom';
import AAA from './a';
import BBB from './b';
import CCC from './c';

const Routers: FC = memo(() => {
  const routes = {
    a: {
      path: `a`,
      component: './a',
    },
    b: {
      path: `b`,
      component: './b',
    },
    c: {
      path: `c`,
      component: './c',
    },
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route path="b" element={<BBB />}></Route>
        <Route path="a" element={<AAA />}></Route>
        <Route path="c" element={<CCC />}></Route>
        <Route path="*" element={<Navigate to={'a'} />}></Route>
      </Routes>
    </BrowserRouter>
  );
});
export default Routers;
