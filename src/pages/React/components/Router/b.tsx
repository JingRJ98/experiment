import React, { FC, memo } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from 'antd';

const BBB: FC = memo(() => {
  const navigate = useNavigate();
  const onClick = () => {
    navigate('/a');
  };
  return (
    <>
      <div>BBB</div>
      <Button onClick={onClick}>去A</Button>
    </>
  );
});
export default BBB;
