import { Button } from 'antd';
import React, { FC, memo } from 'react';
import { useNavigate } from 'react-router-dom';

const AAA: FC = memo(() => {
  const navigate = useNavigate();

  const onClick = () => {
    navigate('/b');
  };

  const onClickC = () => {
    navigate('/c');
  };

  return (
    <>
      <div>AAA</div>
      <Button onClick={onClick}>去B</Button>
      <Button onClick={onClickC}>去C</Button>
    </>
  );
});
export default AAA;
