import React, { FC, memo } from 'react';
import Routers from './Routers';

const Router: FC = memo(() => {
  return (
    <div>
      hello route
      <section>
        <Routers />
      </section>
    </div>
  );
});
export default Router;
