import React, { FC, memo, useEffect } from 'react';

interface TestNavigatorProps {}

const TestNavigator: FC<TestNavigatorProps> = memo(() => {
  useEffect(() => {
    const code = 'header';
    const text = `https://4x-ant-design.antgroup.com/components/modal-cn/#${code}`;

    navigator.clipboard.writeText(text).then(() => {
      console.log(666, '复制成功');
    });
  }, []);

  return <div>TestNavigator</div>;
});
export default TestNavigator;
