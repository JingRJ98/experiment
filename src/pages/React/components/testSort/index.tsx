import { Button } from 'antd';
import React, { FC, memo, useEffect, useState } from 'react';

const TestSort: FC = memo(() => {
  const [list, setList] = useState([]);

  useEffect(() => {
    const l = [
      {
        name: '1',
        order: 1,
      },
      {
        name: '2',
        order: 0,
      },
    ];
    setList(l);
  }, []);

  const onClick = () => {
    [...list].sort((a, b) => {
      console.log('a :>> ', a);
      return a.order - b.order;
    });
  };

  return (
    <div>
      <h2>TestSort</h2>
      {list.map((item) => {
        return <div>{item.name}</div>;
      })}

      <Button onClick={onClick}>点击对list排序</Button>
    </div>
  );
});
export default TestSort;
