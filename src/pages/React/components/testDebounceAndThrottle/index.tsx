import { Input } from 'antd';
import React, { FC, memo, useEffect, useState } from 'react';
import styles from './style.module.scss';

const fun = (cb: () => any, delay: number) => {
  let timer: any = null;
  return (...args: any[]) => {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      cb?.call(this, ...args);
    }, delay);
  };
};

const Index: FC = memo(() => {
  const useDebounce = (fn, delay) => {
    console.log('渲染');

    return fun(fn, delay);
  };

  const [value, setValue] = useState(0);
  console.log('value', value);
  const onChange = useDebounce(() => {
    setValue(value + 1);
  }, 1000);

  return (
    <div>
      {/* <Input value={value} onChange={onChange} style={{ width: 450, marginRight: 20 }} /> */}
      <div onClick={onChange} className={styles.inner}>
        {value}
      </div>
    </div>
  );
});
export default Index;
