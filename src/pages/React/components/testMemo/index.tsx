// components/parent-component.js
import { useState, useEffect } from 'react';
import Counts from './child';
import { Button } from 'antd';
// improt重命名
import { a as constansts } from './constants';
const { MOZARELLA, CHEDDAR, PARMESAN, CABERNET, CHARDONAY } = constansts;

const ParentComponent = () => {
  const [cheeseType, setCheeseType] = useState('');
  const [wine, setWine] = useState('');
  const whichWineGoesBest = () => {
    switch (cheeseType) {
      case MOZARELLA:
        return setWine(CABERNET);
      case CHEDDAR:
        return setWine(CHARDONAY);
      default:
        CHARDONAY;
    }
  };
  useEffect(() => {
    let mounted = true;
    if (mounted) {
      whichWineGoesBest();
    }
    return () => (mounted = false);
  }, [cheeseType]);

  return (
    <div className="flex flex-col justify-center items-center">
      <h3 className="text-center dark:text-gray-400 mt-10">
        Without React.memo() or useMemo()
      </h3>
      <h1 className="font-semibold text-2xl dark:text-white max-w-md text-center">
        Select a cheese and we will tell you which wine goes best!
      </h1>
      <div className="flex flex-col gap-4 mt-10">
        <Button text={MOZARELLA} onClick={() => setCheeseType(MOZARELLA)} />
        <Button text={CHEDDAR} onClick={() => setCheeseType(CHEDDAR)} />
        <Button text={PARMESAN} onClick={() => setCheeseType(PARMESAN)} />
      </div>
      {cheeseType && (
        <p className="mt-5 dark:text-green-400 font-semibold">
          For {cheeseType}, <span className="dark:text-yellow-500">{wine}</span>{' '}
          goes best.
        </p>
      )}
      <Counts />
    </div>
  );
};

export default ParentComponent;
