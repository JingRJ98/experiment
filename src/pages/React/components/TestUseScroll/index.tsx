import { RefObject, useEffect, useRef, useState } from 'react';
import { log } from '@/utils';

const useScrolling = (ref: RefObject<HTMLElement>): boolean => {
  const [scrolling, setScrolling] = useState<boolean>(false);
  useEffect(() => {
    if (ref.current) {
      let scrollingTimer: NodeJS.Timeout;
      const handleScrollEnd = () => {
        setScrolling(false);
      };

      const handleScroll = () => {
        log('正在滚动中');
        setScrolling(true);
        // 防抖
        if (scrollingTimer) {
          clearTimeout(scrollingTimer);
        }
        // 一旦开始滚动 150ms后会设为不在滚动
        // 下一次滚动会打断这个设为不在滚动的操作
        scrollingTimer = setTimeout(() => {
          handleScrollEnd();
        }, 150);
      };

      ref.current?.addEventListener('scroll', handleScroll);

      return () => {
        if (ref.current) {
          ref.current?.removeEventListener('scroll', handleScroll);
        }
      };
    }
  }, [ref]);

  return scrolling;
};

const TestUseScroll = () => {
  const scrollRef = useRef(null);

  const scrolling = useScrolling(scrollRef);

  return (
    <div>
      {<div>{scrolling ? '滚动中..' : '没有滚动'}</div>}

      <div ref={scrollRef} style={{ height: '200px', overflow: 'auto' }}>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
        <div>JRJ</div>
      </div>
    </div>
  );
};

export default TestUseScroll;
