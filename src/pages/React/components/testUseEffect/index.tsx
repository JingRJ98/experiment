import { Button } from 'antd';
import React, { FC, memo, useEffect, useRef, useState } from 'react';

interface IndexProps {}

const Index: FC<IndexProps> = memo((props) => {
  const [s1, setS1] = useState(0);
  const [s2, setS2] = useState(0);
  const s3 = useRef(0);

  useEffect(() => {
    console.log(s1);
    console.log(s2);
    console.log(s3.current);
  }, [s1, s3.current]);

  return (
    <div>
      <div>状态1是{s1}</div>
      <Button onClick={() => setS1((s) => s + 1)}>给状态1加一</Button>
      <div>状态2是{s2}</div>
      <Button onClick={() => setS2((s) => s + 1)}>给状态2加一</Button>
      <Button
        onClick={() => {
          s3.current++;
        }}
      >
        给状态3加一
      </Button>
    </div>
  );
});

export default Index;
