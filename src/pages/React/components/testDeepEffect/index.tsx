import { useRef, useEffect } from 'react';
import type { DependencyList } from 'react';
import { isEqual } from 'lodash-es';

const useDeepEffect: typeof useEffect = (cb, deps) => {
  const ref = useRef<DependencyList>();
  const signalRef = useRef<number>(0);

  if (deps === undefined || !isEqual(deps, ref.current)) {
    ref.current = deps;
    signalRef.current += 1;
  }

  useEffect(cb, [signalRef.current]);
};
