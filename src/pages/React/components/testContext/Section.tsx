import React, { createContext, FC, memo, useContext } from 'react';

import styles from './style.module.scss';

export const LevelContext = createContext(1);

const Section: FC = memo(({ children }) => {
  const level = useContext(LevelContext);

  return (
    <LevelContext.Provider value={level + 1}>
      <div>{children}</div>
    </LevelContext.Provider>
  );
});
export default Section;
