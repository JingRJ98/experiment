import React, { FC, memo, useContext } from 'react';

import { LevelContext } from './Section';

import styles from './style.module.scss';

const Child: FC = memo(() => {
  const level = useContext(LevelContext);
  return <div>{level}</div>;
});
export default Child;
