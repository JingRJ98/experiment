import React, { FC, memo, createContext } from 'react';
import Child from './Child';
import Section from './Section';

import styles from './style.module.scss';

interface TestContextProps {}

export const LevelContext = createContext(1);

const TestContext: FC<TestContextProps> = memo((props) => {
  const {} = props;

  return (
    <div>
      <Section>
        <Child />
        <Child />
        <Child />
        <Section>
          <Child />
          <Child />
          <Child />
          <Section>
            <Child />
            <Child />
            <Child />
          </Section>
        </Section>
      </Section>
    </div>
  );
});
export default TestContext;
