import { initTasks, ITask } from '@/pages/React/components/testUseReducer';

interface ReducerAction extends ITask {
  type: string;
}
export const handleTasks = (state: ITask[], action: ReducerAction): ITask[] => {
  switch (action.type) {
    case 'add': {
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          done: false,
        },
      ];
    }
    case 'done': {
      return [
        ...state,
        {
          done: action.done,
        },
      ];
    }
    default: {
      return initTasks;
    }
  }
};
