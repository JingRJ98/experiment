import React, { FC, memo, useCallback, useReducer, useState } from 'react';
import { Button, Checkbox, Input } from 'antd';

import { handleTasks } from '@/pages/React/components/testUseReducer/reduer';

import styles from './style.module.scss';

export interface ITask {
  id?: number;
  text?: string;
  done?: boolean;
}

export const initTasks: ITask[] = [];
const TestUseReducer: FC = memo(() => {
  const [taskName, setTaskName] = useState<string>('');
  const [tasks, dispatchTasks] = useReducer(handleTasks, initTasks);

  const handleChangeText = useCallback((e) => {
    setTaskName(e.target.value);
  }, []);

  const handleAddTask = useCallback(() => {
    dispatchTasks({
      type: 'add',
      id: Date.now(),
      text: taskName,
    });
    setTaskName('');
  }, [taskName]);

  const handleDoneTask = useCallback((e) => {
    console.log('e.target.value :>> ', e.target.checked);
    dispatchTasks({
      type: 'done',
    });
  }, []);
  return (
    <div>
      <div>
        <Input
          placeholder="输入待办事项"
          value={taskName}
          onChange={handleChangeText}
        />
        <Button onClick={handleAddTask}>Add</Button>
      </div>
      <div>
        {tasks.map((item: ITask) => (
          <div key={item.id}>
            <Checkbox checked={item.done} onChange={handleDoneTask} />
            {item.text}
          </div>
        ))}
      </div>
    </div>
  );
});

export default TestUseReducer;
