import React, { FC, memo, useState } from 'react';
import usePrevious from '../../../../hooks/usePrevious';
import { Button } from 'antd';

const Index: FC = memo((props) => {
  const [state, setState] = useState(0);

  const previousState = usePrevious(state);

  return (
    <div>
      <Button onClick={() => setState((v) => v + 1)}>+1</Button>
      <div> 之前的previousState: {previousState}</div>
      <div> 现在的state: {state}</div>
    </div>
  );
});
export default Index;
