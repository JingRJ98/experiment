import { Button, Card } from 'antd';
import { useState, FC } from 'react';

const TestUseState: FC = () => {
  const [count, setCount] = useState(0);
  const onClick = () => {
    setCount(count + 1);
    setCount((count) => count + 1);
    setCount(count + 1);
    setCount((count) => count + 1);
  };

  const [count1, setCount1] = useState(0);
  const onClick1 = () => {
    setCount1(count1 + 1);
    setCount1(count1 + 1);
    setCount1(count1 + 1);
  };

  const [count2, setCount2] = useState(0);
  const onClick2 = () => {
    setTimeout(() => {
      setCount2(count2 + 1);
      setCount2(count2 + 1);
      setCount2(count2 + 1);
    });
  };

  console.log(7);

  return (
    <div>
      <Button onClick={onClick}>点击新增</Button>
      <Button onClick={onClick1}>点击新增</Button>
      <Button onClick={onClick2}>点击新增</Button>
      <Card>情况一{count}</Card>
      <Card>情况二{count1}</Card>
      <Card>情况三{count2}</Card>
    </div>
  );
};

export default TestUseState;
