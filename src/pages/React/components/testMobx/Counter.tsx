import React, { FC, memo, useState } from 'react';
import { observer } from 'mobx-react-lite';

import counterStore from './store'


const Counter: FC = observer(() => {
    const [s, setS] = useState(0)

    const onClick1 = () => {
        setS(s + 1)
        setS(s + 1)
    }
    const onClick2 = () => {
        setS(p => p + 1)
        setS(p => p + 1)
    }
    return (
        <div>
            <p>{s}</p>
            <button onClick={onClick1}>click1</button>
            <button onClick={onClick2}>click2</button>
            <p>当前数字是: {counterStore.count}</p>
            <div onClick={counterStore.increment}>+1</div>
            <div onClick={counterStore.decrement}>-1</div>
            <div onClick={counterStore.reset}>重置</div>
        </div>
    )
});

export default Counter;