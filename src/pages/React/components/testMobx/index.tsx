import React, { FC, memo } from 'react';

import Counter from './Counter';

interface TestMobxProps { }

const TestMobx: FC<TestMobxProps> = memo((props) => {
    const { } = props;

    return (
        <div>
            <Counter></Counter>
        </div>
    )
});

export default TestMobx;