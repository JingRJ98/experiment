import { makeAutoObservable, observable } from "mobx";

class CounterStore {
    count = 10;

    constructor() {
        // makeAutoObservable(this);
        makeAutoObservable(this, {}, { autoBind: true });
    }

    increment() {
        this.count += 1
    }
    decrement() {
        this.count -= 1
    }

    reset(){
        this.count = 10
    }
}

export default new CounterStore();