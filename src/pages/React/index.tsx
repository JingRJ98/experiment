import React from 'react';
import TestContext from './components/testContext';
import TestFowardRef from './components/testFowardRef';
import TestUseState from './components/testUseState';
import TestDebounceAndThrottle from './components/testDebounceAndThrottle';
import TestWebWorker from './components/testWebWorker';
import TestNavigator from './components/testNavigator';
import TestUseTransition from './components/testUseTransition';
import TestUseScroll from './components/TestUseScroll';
import TestDecorator from './components/testDecorator';
import TestUsePrevious from './components/testUsePrevious';
import TestUseEffect from './components/testUseEffect';
import TestSort from './components/testSort';
import Router from './components/Router';
import TestUseRef from './components/testUseRef';
import TestMobx from './components/testMobx';

const TestReact: React.FC = React.memo(() => (
  <>
    {/* <TestUseState></TestUseState> */}
    {/* <TestFowardRef></TestFowardRef> */}
    {/* <TestContext></TestContext> */}
    {/* <TestDebounceAndThrottle></TestDebounceAndThrottle> */}
    {/* <TestWebWorker></TestWebWorker> */}
    {/* <TestNavigator></TestNavigator> */}
    {/* <TestUseTransition></TestUseTransition> */}
    {/* <TestUseScroll></TestUseScroll> */}
    {/* <TestDecorator></TestDecorator> */}
    {/* <TestUsePrevious></TestUsePrevious> */}
    {/* <TestUseEffect></TestUseEffect> */}
    {/* <TestSort></TestSort> */}
    {/* <Router></Router> */}
    {/* <TestUseRef></TestUseRef> */}
    <TestMobx></TestMobx>
  </>
));
export default TestReact;
