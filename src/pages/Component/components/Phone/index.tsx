import React, { FC, memo } from 'react';
import { Input, Button } from 'antd-mobile/2x';
import VConsole from 'vconsole';

import styles from './style.module.scss';

interface PhoneProps {}
new VConsole();
const Phone: FC<PhoneProps> = memo((props) => {
  const {} = props;

  console.log('navigator.userAgent :>> ', navigator.userAgent);

  if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
    console.log('isIOS');
  } else if (/(Android)/i.test(navigator.userAgent)) {
    console.log('isAndroid');
  } else {
    console.log('isPC');
  }

  return (
    <div className={styles.container}>
      <h2>Hello iPhone</h2>
      <div className={styles.inner}>1</div>
      <div className={styles.inner1}>2</div>
      <Input placeholder="请输入"></Input>
    </div>
  );
});
export default Phone;
