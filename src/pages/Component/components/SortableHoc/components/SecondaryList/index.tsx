import { FC, memo, useCallback } from 'react';
import cx from 'classnames';

import styles from './style.module.scss';
import { Checkbox } from 'antd';

interface SecondaryListProps {
  visible: boolean;
  data: any[];
}

const SecondaryList: FC<SecondaryListProps> = memo((props) => {
  const { visible, data } = props;

  const onchange = useCallback((e, v) => {
    console.log('e :>> ', e);
    console.log('v :>> ', v);
  }, []);

  return (
    <div
      className={cx(styles.container, {
        [styles.hide]: !visible,
      })}
    >
      {data.map(({ label, value, checked }) => (
        <li style={{ zIndex: 9999, listStyle: 'none' }}>
          <Checkbox
            defaultChecked={checked}
            onChange={(e) => onchange(e, value)}
          >
            {label}
          </Checkbox>
        </li>
      ))}
    </div>
  );
});
export default SecondaryList;
