import React, { FC, memo, useCallback, useMemo } from 'react';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';

import styles from './style.module.scss';
import { Button, Checkbox } from 'antd';
import SecondaryList from './components/SecondaryList';

interface SortableListProps {}

const SortableList: FC<SortableListProps> = memo(() => {
  const tagList = useMemo(
    () => [
      {
        value: 0,
        checked: true,
        children: [
          { label: '测试111', value: `0-1`, checked: true },
          { label: '2222', value: `0-2`, checked: true },
          { label: '333333', value: `0-3`, checked: true },
        ],
        label: '曾经',
      },
      {
        value: 1,
        checked: true,
        children: [
          { label: '测试111', value: `1-1`, checked: true },
          { label: '2222', value: `1-2`, checked: true },
          { label: '333333', value: `1-3`, checked: true },
        ],
        label: '沧海',
      },
      {
        value: 2,
        checked: true,
        children: [
          { label: '测试111', value: `2-1`, checked: true },
          { label: '2222', value: `2-2`, checked: true },
          { label: '333333', value: `2-3`, checked: true },
        ],
        label: '沧海难为水',
      },
      {
        value: 3,
        checked: true,
        children: [
          { label: '测试111', value: `3-1`, checked: true },
          { label: '2222', value: `3-2`, checked: true },
          { label: '333333', value: `3-3`, checked: true },
        ],
        label: '地方浮点数',
      },
    ],
    [],
  );

  const onChange = useCallback((v, value) => {
    console.log('v :>> ', v);
    console.log('value :>> ', value);
  }, []);

  const handleExpand = useCallback((e, v) => {
    console.log('e :>> ', e);
    console.log('v :>> ', v);
  }, []);

  const Item = useMemo(() => {
    return SortableElement(({ value }) => {
      console.log('value :>> ', value);

      return (
        <li style={{ zIndex: 9999, listStyle: 'none' }}>
          <div style={{ color: '#fff' }}>
            <Checkbox
              defaultChecked={value.checked}
              onChange={(v) => onChange(v, value)}
            >
              {value.label}
            </Checkbox>
            <Button onClick={(e) => handleExpand(e, value.value)}>展开</Button>
          </div>
          <SecondaryList visible={true} data={value.children}></SecondaryList>
          {/* <div style={{ zIndex: 9999, fontSize: '36px'}}>SecondaryList</div> */}
        </li>
      );
    });
  }, [handleExpand, onChange]);

  const Container = useMemo(() => {
    return SortableContainer(({ items }) => {
      return (
        <ul>
          {items.map((item, index) => (
            <Item key={item.value} index={index} value={item}></Item>
          ))}
        </ul>
      );
    });
  }, [Item]);

  return (
    <div className={styles.container}>
      <Container items={tagList}></Container>
    </div>
  );
});
export default SortableList;
