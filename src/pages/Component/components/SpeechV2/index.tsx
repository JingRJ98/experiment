import { FC, memo, useRef, useState } from 'react';
import styles from './style.module.scss';
import recorder from './utils/recorder';
import { Button } from 'antd';
import SpeechBar from './SpeechBar';



/**
 * 随机生成x到y之间的整数 包含x和y
 * @param {number} x
 * @param {number} y
 * @returns {number}
 */
const getRandom = (x, y) => Math.floor(Math.random() * (y - x + 1) + x)

// 有权重的随机值获取函数
function getRandomWithProbability(options: number[], probabilities: number[]): number {
    // 生成一个随机数
    const random = Math.random();

    let cumulativeProbability = 0;
    for (let i = 0; i < probabilities.length; i++) {
        cumulativeProbability += probabilities[i];
        if (random < cumulativeProbability) {
            return options[i];
        }
    }
    return options[options.length - 1];
}

const options = [6, 8, 12, 15, 18];
const vol3To20 = [0.1, 0.1, 0.4, 0.3, 0.1];
const vol20To40 = [0.1, 0.2, 0.1, 0.4, 0.2];
const vol40To60 = [0, 0.1, 0.2, 0.3, 0.4];
const vol60To80 = [0, 0.1, 0.1, 0.3, 0.5];
const vol80To100 = [0, 0, 0.1, 0.2, 0.7];

// 在arr中随机插入c个 间隔, 间隔为随机的2
const insertDivide = (lines: waveLines, count: number) => {
    for (let i = 0; i < count; i++) {
        const index = getRandom(1, lines.length - 2);
        const [first, second] = [lines[index], lines[index + 1]]
        first.height = 4
        second.height = 4
        lines.splice(index, 2, ...[first, second])
    }
}

// 生成一个较为自然的声波跳动效果
// vol 音量 0-100
const generateWaveDance = (vol: number, lines: waveLines): waveLines => {
    const length = lines.length
    const newLines = [...lines]
    // 左右偏移
    const offset = getRandom(0, 5)

    // vol越高 理论上更大概率生成更高的随机声音
    if (vol < 3) {
        const idx = getRandom(0, length - 1)
        for (let i = 0; i < length; i++) {
            if (i === idx - 2 || i === idx + 2) {
                newLines[i].height = 6
            } else if (
                i === idx - 1 || i === idx + 1
            ) {
                newLines[i].height = 8
            } else if (i === idx) {
                newLines[i].height = 10
            } else {
                newLines[i].height = 4
            }
        }
    } else if (vol >= 3 && vol < 20) {
        for(let i = 0;i< length;i++){
            newLines[i].height = getRandomWithProbability(options, vol3To20)
        }
        insertDivide(newLines, 9)
    } else if (vol >= 20 && vol < 40) {
        for(let i = 0;i< length;i++){
            newLines[i].height = getRandomWithProbability(options, vol20To40)
        }
        insertDivide(newLines, 8)
    } else if (vol >= 40 && vol < 60) {
        for(let i = 0;i< length;i++){
            newLines[i].height = getRandomWithProbability(options, vol40To60)
        }
        insertDivide(newLines, 6)
    } else if (vol >= 60 && vol < 80) {
        for(let i = 0;i< length;i++){
            newLines[i].height = getRandomWithProbability(options, vol60To80)
        }
        insertDivide(newLines, 4)
    } else if (vol >= 80 && vol <= 100) {
        for(let i = 0;i< length;i++){
            newLines[i].height = getRandomWithProbability(options, vol80To100)
        }
        insertDivide(newLines, 2)
    }


    return newLines
}










type waveLines = { x: number, height: number }[]

const originalLines: waveLines = []
for (let i = 0; i < 60; i++) {
    originalLines.push({
        x: i * 6,
        height: 4
    })
}

const Speech: FC = memo(() => {
    const [data, setData] = useState({
        duration: 0,
        fileSize: 0,
        vol: 0
    })
    const [timeDown, setTimeDown] = useState(59)
    const [lines, setLines] = useState<waveLines>(originalLines);

    const timer = useRef<NodeJS.Timeout | null>(null);
    const volRef = useRef<number>(0);
    const freRef = useRef<number>(0);

    const onStartRecord = () => {
        recorder.onprogress = (params) => {
            // console.log('params :>> ', params);
            volRef.current = Math.floor(params.vol);
            freRef.current = freRef.current + 1
            //
            if (freRef.current % 3 === 0) {
                // 降低频率, 每5次收音操作才执行一次绘制声波图
                freRef.current = 0
                // addLine(Math.floor(params.vol))
                generateRandomLine(Math.floor(params.vol))
            }
            setData({
                duration: Number(params.duration.toFixed(5)),
                fileSize: params.fileSize,
                vol: Number(params.vol.toFixed(2))
            })
        }
        recorder.start().then(() => {
            console.log('开始录音');
            // 开始倒计时
            let timeDown = 59;
            timer.current = setInterval(() => {
                timeDown--;
                if (timeDown < 0) {
                    timer.current && clearInterval(timer.current);
                    onEndRecord();
                    // 打印出录制已完成的字
                } else {
                    setTimeDown(timeDown);
                }
            }, 1000);
        }, (error) => {
            console.log(`异常了,${error}`);
        });
        // 开始绘制canvas
        // drawRecord();
    }

    const onPauseRecord = () => {
        recorder.pause();
        console.log('暂停录音');
    }
    const onResumeRecord = () => {
        recorder.resume();
        console.log('恢复录音');
    }

    const onEndRecord = () => {
        recorder.stop();
        console.log('结束录音');
        timer.current && clearInterval(timer.current);
        // drawRecordId && cancelAnimationFrame(drawRecordId);
        // drawRecordId = null;
    }

    // 断开麦克风连接, 理论上结束录音的时候就应该自动断开连接
    const onDestroyRecord = () => {
        recorder.destroy().then(() => {
            setData({
                duration: 0,
                fileSize: 0,
                vol: 0
            })
            console.log('销毁实例');
            setLines(originalLines)
            setTimeDown(59)
        });
    }

    const onDownloadPCM = () => {
        console.log('pcm: ', recorder.getPCMBlob());
        recorder.downloadPCM();
    }

    const onDownloadWAV = () => {
        console.log('wav: ', recorder.getWAVBlob());
        recorder.downloadWAV();
    }

    const logBase64 = async () => {
        const res = await recorder.getPCMBlobBase64()
        console.log('res: ', res);
    }

    const generateRandomLine = (vol) => {
        console.log('vol :>> ', vol);
        const newLines = generateWaveDance(vol, lines)

        setLines(newLines);
    }

    const onClose = () => {
        console.log('关闭, pcm: ', recorder.getPCMBlob());
        // 其他操作
        onDestroyRecord()
    }

    return (
        <div className={styles.container}>
            <div style={{ marginLeft: 40 }}>SpeechV2测试</div>
            <div className={styles.outter}>
                <div className={styles.btns}>
                    <button onClick={onStartRecord}>开始录音</button>
                    <button onClick={onPauseRecord}>暂停录音</button>
                    <button onClick={onResumeRecord}>恢复录音</button>
                    <button onClick={onEndRecord}>结束录音</button>
                    <button onClick={onDestroyRecord}>断连</button>
                </div>
                <div className={styles.realtimeData}>
                    <div>录音时长(秒): {data.duration}</div>
                    <div>录音大小(kb): {data.fileSize / 1000}</div>
                    <div>录音音量百分比: : {data.vol} %</div>
                </div>
                <div className={styles.btns}>
                    <button onClick={onDownloadPCM}>下载PCM</button>
                    <button onClick={onDownloadWAV}>下载WAV</button>
                    <button onClick={logBase64}>Base64</button>
                    {/* <button onClick={onDownloadMP3}>下载mp3</button> */}
                </div>

                <div className={styles.waveContainer}>
                    <div className={styles.wave}>
                        <svg
                            width="400px"
                            height="100px"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            {/* 绘制竖线 */}
                            {lines.map((line) => (
                                <rect
                                    key={line.x}
                                    className={styles.rect}
                                    x={line.x}
                                    y={50 - line.height / 2}
                                    height={line.height}
                                    width="2"
                                    rx="1"
                                    fill='rgba(73, 85, 245, 0.80)'
                                />
                            ))}
                        </svg>
                    </div>
                    <div className={styles.countdown}>00:{timeDown}</div>
                    <Button className={styles.close} onClick={onClose}>关闭按钮</Button>
                </div>

                <SpeechBar node={{}} />
            </div>
        </div>
    );
});
export default Speech;
