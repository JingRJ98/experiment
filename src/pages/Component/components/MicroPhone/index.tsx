import React, { FC, memo, useEffect, useRef, useState } from 'react';
import MicroPhoneComp from './microphone'

// import styles from './index.module.scss';

interface MicroPhoneProps {
    // v: number
}

const MicroPhone: FC<MicroPhoneProps> = memo(() => {
    const [v, setV] = useState(0)
    const timer = useRef<null | NodeJS.Timeout>(null)

    useEffect(() => {
        const move = () => {
            const r = (Math.random() * 100) >> 0
            setV(r)
            // requestAnimationFrame(move)
        }
        document.addEventListener('click', () => {
            // timer.current && clearInterval(timer.current)
            // requestAnimationFrame(move)
            timer.current = setInterval(() => {
                move()
            }, 200);
        })

        return () => {
            timer.current && clearInterval(timer.current)
        }
    }, [])

    return (
        <div>
            <MicroPhoneComp v={v}></MicroPhoneComp>
        </div>
    )
});

export default MicroPhone;
