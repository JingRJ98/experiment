import React, { FC, memo, useEffect, useMemo, useRef, useState } from 'react';

import styles from './index.module.scss';

interface MicroPhoneProps {
    v: number
}

const MicroPhone: FC<MicroPhoneProps> = memo((props) => {
    const { v } = props;

    const volumeValue = useMemo(() => {
        return v
    }, [v])


    return (
        <>
         <div className={styles.tipContainer}>
            <div className={styles.title}>最多可上传4个文件</div>
            {/* 文件类型名和/之间需要加空格, 因为文字换行是按照一个单词换行 */}
            {/* 例子: 支持 JPG / JPEG / PNG */}
            <div className={styles.txt}><span className={styles.secTitle}>图片：</span>不超过10MB，支持 JPG / JPEG / PNG / WEBP / BMP / ICO</div>
            <div className={styles.txt}><span className={styles.secTitle}>文档：</span>不超过200MB，支持 Word / PDF / TXT / Excel / PPT / URL</div>
            <div className={styles.txt}><span className={styles.secTitle}>视频：</span>不超过100MB，支持 MP4 / MKV / MOV / FLV / F4V / VOB / MPEG / AVI / WMV / RMVB</div>
            <div className={styles.txt}><span className={styles.secTitle}>音频：</span>不超过25MB，支持 mp3 / m4a / wav / webm</div>
        </div>
            <div className={styles.container}>
                <div>{v}</div>
                {/* <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
                <path d="
                M7.49986 9.5
                C7.49986 5.91015 10.41 3 13.9999 3
                C17.5897 3 20.4999 5.91015 20.4999 9.5
                V14.5
                C20.4999 18.0899 17.5897 21 13.9999 21
                C10.41 21 7.49986 18.0899 7.49986 14.5
                V9.5
                Z
                " fill="#E1E1E6" />
                <path d="M6.31007 14.8999C6.29878 14.6793 6.12078 14.5 5.89986 14.5H4.89986C4.67895 14.5 4.499 14.6796 4.50815 14.9003C4.70549 19.6608 8.40596 23.5166 13.0995 23.9579V25.6C13.0995 25.8209 13.2786 26 13.4995 26H14.4995C14.7204 26 14.8995 25.8209 14.8995 25.6V23.958C19.5934 23.5171 23.2942 19.661 23.4916 14.9003C23.5007 14.6796 23.3208 14.5 23.0999 14.5H22.0999C21.879 14.5 21.7009 14.6793 21.6897 14.8999C21.4816 18.9666 18.1183 22.2 13.9999 22.2C9.88139 22.2 6.51812 18.9666 6.31007 14.8999Z" fill="#E1E1E6" />
                </svg> */}

                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
                    {/* 音量容器部分 */}
                    <path d="
                    M7.5 9.5
                    C7.5 5.91015 10.4101 3 14 3
                    C17.5899 3 20.5 5.91015 20.5 9.5
                    V14.5
                    C20.5 18.0899 17.5899 21 14 21
                    C10.4101 21 7.5 18.0899 7.5 14.5
                    V9.5
                    Z
                    " fill="#bd334c" />


                    {/* 音量条 */}
                    <path d="
                    M7.5 9.5
                    H20.5
                    V14.5
                    L20.5 14.5
                    C20.5 18.0899 17.5899 21 14 21
                    C10.4101 21 7.5 18.0899 7.5 14.5
                    L7.5 12
                    Z
                    " fill="#13BF3E" />

                    {/* 麦克风底座 */}
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.31007 14.8999C6.29878 14.6793 6.12078 14.5 5.89986 14.5H4.89986C4.67895 14.5 4.499 14.6796 4.50815 14.9003C4.70549 19.6608 8.40596 23.5166 13.0995 23.9579V25.6C13.0995 25.8209 13.2786 26 13.4995 26H14.4995C14.7204 26 14.8995 25.8209 14.8995 25.6V23.958C19.5934 23.5171 23.2942 19.661 23.4916 14.9003C23.5007 14.6796 23.3208 14.5 23.0999 14.5H22.0999C21.879 14.5 21.7009 14.6793 21.6897 14.8999C21.4816 18.9666 18.1183 22.2 13.9999 22.2C9.88139 22.2 6.51812 18.9666 6.31007 14.8999Z" fill="#E1E1E6" />
                </svg>


                <div className={styles.volContainer}>
                    <div className={styles.volume}>
                        <div className={styles.inner}
                            style={{
                                height: `${volumeValue}px`
                            }}
                        />
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.31007 14.8999C6.29878 14.6793 6.12078 14.5 5.89986 14.5H4.89986C4.67895 14.5 4.499 14.6796 4.50815 14.9003C4.70549 19.6608 8.40596 23.5166 13.0995 23.9579V25.6C13.0995 25.8209 13.2786 26 13.4995 26H14.4995C14.7204 26 14.8995 25.8209 14.8995 25.6V23.958C19.5934 23.5171 23.2942 19.661 23.4916 14.9003C23.5007 14.6796 23.3208 14.5 23.0999 14.5H22.0999C21.879 14.5 21.7009 14.6793 21.6897 14.8999C21.4816 18.9666 18.1183 22.2 13.9999 22.2C9.88139 22.2 6.51812 18.9666 6.31007 14.8999Z" fill="#E1E1E6" />
                    </svg>
                </div>
            </div>
        </>
    )
});

export default MicroPhone;
