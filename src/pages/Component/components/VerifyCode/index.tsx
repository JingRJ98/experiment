import React, { FC, memo, useEffect, useRef } from 'react';

import './style.scss';

interface IndexProps {}

const VerifyCode: FC<IndexProps> = memo((props) => {
  const codeContainer = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (codeContainer.current) {
      codeContainer.current.addEventListener('input', (e) => {
        // 限制每个输入框只能输入一个数字
        if (e.target.value.length) {
          const s = e.target.value.slice(0, 1);
          e.target.value = s;

          for (const [i, v] of [...codeContainer.current.children].entries()) {
            if (e.target === v && i !== 5) {
              // 找到点击的是第几个子元素
              codeContainer.current.children[i + 1].focus();
            }
          }
        }
      });

      codeContainer.current.addEventListener('keydown', (e) => {
        if (e.key !== 'Backspace') {
          return;
        }
        // 先删除本框的数字
        if (!e.target.value.length) {
          for (const [i, v] of [...codeContainer.current.children].entries()) {
            if (e.target === v && i !== 0) {
              // 找到点击的是第几个子元素
              codeContainer.current.children[i - 1].focus();
            }
          }
        }
        return;
      });
    }
  }, []);

  return (
    <div>
      <div>验证码</div>
      <div className="varify-code-container" ref={codeContainer}>
        <input type="text" className="code_input" />
        <input type="text" className="code_input" />
        <input type="text" className="code_input" />
        <input type="text" className="code_input" />
        <input type="text" className="code_input" />
        <input type="text" className="code_input" />
      </div>
    </div>
  );
});
export default VerifyCode;
