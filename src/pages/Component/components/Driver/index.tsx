import { FC, memo, useCallback, useEffect, useMemo } from 'react';

import { driver } from 'driver.js';
import 'driver.js/dist/driver.css';

import styles from './style.module.scss';

// dirver的基础配置和hightlight函数
const Driver: FC = memo(() => {
  const handleHightLight = useCallback(() => {
    const driverObj = driver({
      popoverClass: 'driverjs-theme',
      overlayColor: '#000',
      overlayOpacity: 0.65,
      stagePadding: 10,
      stageRadius: 10,
    });
    driverObj.highlight({
      element: '#some-element',
      popover: {
        title: 'Title',
        description: 'Description',
        side: 'top',
        align: 'center',
      },
    });
  }, []);

  const menu = useMemo(() => {
    const menu = [];
    for (let i = 0; i < 10; i++) {
      const random = (Math.random() * 27) >> 0;
      const s = String.fromCharCode('a'.charCodeAt(0) + random);
      menu.push(s + (Date.now() + Math.random() * 107).toString(36));
    }
    return menu;
  }, []);

  useEffect(() => {
    handleHightLight();
  }, [handleHightLight]);

  return (
    <div className={styles.container}>
      <h2>Dirver Test</h2>
      <div className={styles.content}>
        <div className={styles.slider}>
          {menu.map((v, i) => (
            <div key={i}>{v}</div>
          ))}
        </div>
        <div className={styles.main}>
          <div id="some-element">1</div>
          <div>2</div>
          <div>3</div>
        </div>
      </div>
    </div>
  );
});
export default Driver;
