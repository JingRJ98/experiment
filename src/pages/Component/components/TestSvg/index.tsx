import React, { FC, memo, useEffect, useState } from 'react';

// import {
//     FileUploadIcon
// } from './assets'

import styles from './index.module.scss';
import { Button } from 'antd';

interface TestSvgProps { }

const icon = (
    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
        <path fill-rule="evenodd" clip-rule="evenodd"
            d="M19.7201 17.5895C20.2175 16.6706 20.4999 15.6183 20.4999 14.5V9.5C20.4999 5.91015 17.5897 3 13.9999 3C11.5969 3 9.49844 4.30397 8.37355 6.24291L6.34781 4.21717C6.1916 4.06096 5.93833 4.06096 5.78212 4.21717L5.21644 4.78285C5.06023 4.93906 5.06023 5.19233 5.21644 5.34854L23.6508 23.7829C23.807 23.9391 24.0602 23.9391 24.2164 23.7829L24.7821 23.2172C24.9383 23.061 24.9383 22.8077 24.7821 22.6515L21.9034 19.7728C22.8425 18.368 23.417 16.6987 23.4916 14.9003C23.5007 14.6796 23.3208 14.5 23.0999 14.5H22.0999C21.879 14.5 21.7009 14.6793 21.6897 14.8999C21.6231 16.2011 21.2335 17.4169 20.5995 18.4689L19.7201 17.5895ZM7.49986 9.63197L17.7075 19.8396C16.6559 20.5711 15.378 21 13.9999 21C10.41 21 7.49986 18.0899 7.49986 14.5V9.63197ZM19.8519 21.9841L18.5675 20.6996C17.2896 21.6426 15.7099 22.2 13.9999 22.2C9.88139 22.2 6.51812 18.9666 6.31007 14.8999C6.29878 14.6793 6.12078 14.5 5.89986 14.5H4.89986C4.67895 14.5 4.499 14.6796 4.50815 14.9003C4.70549 19.6608 8.40596 23.5166 13.0995 23.9579V25.6C13.0995 25.8209 13.2786 26 13.4995 26H14.4995C14.7204 26 14.8995 25.8209 14.8995 25.6V23.958C16.7579 23.7834 18.4607 23.0735 19.8519 21.9841Z"
            fill="#E64A45" />
    </svg>

)

const TestSvg: FC<TestSvgProps> = memo(() => {
    const [s, setS] = useState(0)

    console.log('rerender');

    useEffect(() => {
        console.log('副作用函数执行');

        return () => {
            console.log('清理函数执行');
        }
    }, [s])

    return (
        <div>
            <div>index</div>
            <Button onClick={() => setS(s => s + 1)}>click</Button>
            <div className={styles.a}>
                <div>
                    {icon}
                </div>
            </div>
        </div>
    )
});

export default TestSvg;
