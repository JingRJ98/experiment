import { FC, memo, useCallback, useEffect, useMemo, useRef } from 'react';

import { driver } from 'driver.js';
import 'driver.js/dist/driver.css';

import styles from './style.module.scss';

// driver的setp和dirve函数
const Driver: FC = memo(() => {
  const timer = useRef<ReturnType<typeof setTimeout>>();
  const menu = useMemo(() => {
    const menu = [];
    for (let i = 0; i < 10; i++) {
      const random = (Math.random() * 27) >> 0;
      const s = String.fromCharCode('a'.charCodeAt(0) + random);
      menu.push(s + (Date.now() + Math.random() * 107).toString(36));
    }
    return menu;
  }, []);

  const handleHightLight = useCallback(() => {
    const driverObj = driver({
      showProgress: true,
      nextBtnText: '下一步',
      prevBtnText: '上一步',
      doneBtnText: '完成',
      steps: [
        {
          element: '.first',
          popover: { title: 'Title', description: 'Description' },
        },
        {
          element: '.second',
          popover: { title: 'Title', description: 'Description' },
        },
        {
          element: '.third',
          popover: { title: 'Title', description: 'Description' },
        },
        {
          element: '.dummyNode',
          popover: {
            title: '居中显示',
            description: '因为找不到元素所以居中显示一个提示',
          },
        },
      ],
    });

    driverObj.drive();
  }, []);

  useEffect(() => {
    timer.current = setTimeout(() => {
      handleHightLight();
    }, 1000);
    return () => {
      clearTimeout(timer.current);
    };
  }, [handleHightLight]);

  return (
    <div className={styles.container}>
      <h2>Dirver Test</h2>
      <div className={styles.content}>
        <div className={styles.slider}>
          {menu.map((v, i) => (
            <div key={i}>{v}</div>
          ))}
        </div>
        <div className={styles.main}>
          <div className="first">1</div>
          <div className="second">2</div>
          <div className="third">3</div>
        </div>
      </div>
    </div>
  );
});
export default Driver;
