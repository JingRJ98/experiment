import { useEffect, useRef, useState } from "react";

const Comp = () => {
    const [seconds, setSeconds] = useState(30)
    const intervalRef = useRef<{timer: number} | null>(null)

    const mySetInterval = (callback: () => void, delay: number = 0) => {
        let start = new Date().getTime();
        const obj = {
            timer: 0
        }
        const cb = () => {
            const current = new Date().getTime();
            if (current - start >= delay) {
                callback();
                start = new Date().getTime();
            }
            obj.timer = requestAnimationFrame(cb);
        };
        obj.timer = requestAnimationFrame(cb);
        return obj;
    };

    useEffect(() => {
        intervalRef.current = mySetInterval(() => {
            setSeconds(s => s - 1)
        }, 1000);

        return () => {
            intervalRef.current && cancelAnimationFrame(intervalRef.current.timer)
        }
    }, [])

    const callback = () => {
        console.log('倒计时结束，获得奖励')
    }

    useEffect(() => {
        if (seconds <= 0) {
            callback()
            intervalRef.current && cancelAnimationFrame(intervalRef.current.timer)
        }
    }, [seconds])

    return (
        <div>
            观看 {seconds} 秒广告后获得现金奖励！！！
        </div>
    )
};

export default Comp;
