import { Button } from 'antd';
import React, { FC, memo, useRef, useState } from 'react';

interface IndexProps {}

const Index: FC<IndexProps> = memo((props) => {
  const {} = props;

  const count = useRef(14);
  const timer = useRef(null);

  const [s, setS] = useState('');
  const [t, setT] = useState(0);

  const task = () => {
    return new Promise((resolve) => {
      timer.current = setTimeout(() => {
        count.current--;
        console.log('all :>> ', count.current);
        resolve();
      }, 1000);
    });
  };

  const taskRunner = async () => {
    await task();
    if (count.current > 0) {
      if (count.current <= 10) {
        setT(count.current);
      }
      taskRunner();
    } else {
      setS('Hello World');
    }
  };

  const onClick = () => {
    taskRunner();
  };

  const onStop = () => {
    count.current = -1;
    clearTimeout(timer.current);
  };

  console.log('count.current :>> ', count.current);

  return (
    <div>
      <div>
        {s}
        {t > 1 ? `当前剩余时间${t}s` : ''}
      </div>
      <Button onClick={onClick}>点击开始倒计时</Button>
      <div></div>
      <Button onClick={onStop}>点击结束倒计时</Button>
    </div>
  );
});
export default Index;
