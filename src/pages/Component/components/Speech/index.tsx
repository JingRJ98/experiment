import { FC, memo, useRef, useState } from 'react';
import styles from './style.module.scss';
import recorder from './utils/recorder';
import { Button } from 'antd';
import SpeechBar from './SpeechBar';


const Speech: FC = memo(() => {
    const [data, setData] = useState({
        duration: 0,
        fileSize: 0,
        vol: 0
    })
    const [timeDown, setTimeDown] = useState(59)
    const [lines, setLines] = useState([]);

    const timer = useRef<NodeJS.Timeout | null>(null);
    const volRef = useRef<number>(0);
    const freRef = useRef<number>(0);

    const onStartRecord = () => {
        recorder.onprogress = (params) => {
            // console.log('params :>> ', params);
            // 调整onprogress执行的频率?
            // addLine(params.vol.toFixed(2))
            volRef.current = Math.floor(params.vol);
            freRef.current = freRef.current + 1
            //
            if(freRef.current % 6 === 0) {
                // 降低频率, 每5次收音操作才执行一次绘制声波图
                freRef.current = 0
                addLine(Math.floor(params.vol))
            }
            setData({
                duration: Number(params.duration.toFixed(5)),
                fileSize: params.fileSize,
                vol: Number(params.vol.toFixed(2))
            })
        }
        recorder.start().then(() => {
            console.log('开始录音');
            // 开始倒计时
            let timeDown = 59;
            timer.current = setInterval(() => {
                timeDown--;
                if (timeDown < 0) {
                    timer.current && clearInterval(timer.current);
                    onEndRecord();
                    // 打印出录制已完成的字
                } else {
                    // addLine(volRef.current)
                    setTimeDown(timeDown);
                }
            }, 1000);
        }, (error) => {
            console.log(`异常了,${error}`);
        });
        // 开始绘制canvas
        // drawRecord();
    }

    const onPauseRecord = () => {
        recorder.pause();
        console.log('暂停录音');
    }
    const onResumeRecord = () => {
        recorder.resume();
        console.log('恢复录音');
    }

    const onEndRecord = () => {
        recorder.stop();
        console.log('结束录音');
        timer.current && clearInterval(timer.current);
        // drawRecordId && cancelAnimationFrame(drawRecordId);
        // drawRecordId = null;
    }

    // 断开麦克风连接, 理论上结束录音的时候就应该自动断开连接
    const onDestroyRecord = () => {
        recorder.destroy().then(() => {
            setData({
                duration: 0,
                fileSize: 0,
                vol: 0
            })
            console.log('销毁实例');
            setLines([])
            setTimeDown(59)
        });
    }

    const onDownloadPCM = () => {
        console.log('pcm: ', recorder.getPCMBlob());
        recorder.downloadPCM();
    }

    const onDownloadWAV = () => {
        console.log('wav: ', recorder.getWAVBlob());
        recorder.downloadWAV();
    }

    const logBase64 = async() => {
        const res = await recorder.getPCMBlobBase64()
        console.log('res: ', res);
    }

    // const onDownloadMP3 = () => {
    //     const mp3Blob = convertToMp3(recorder.getWAV());
    //     recorder.download(mp3Blob, 'recorder', 'mp3');
    // }


    // const drawRecord = () => {
    //     // 实时获取音频大小数据
    //     let dataArray = recorder.getRecordAnalyseData()
    //     let bufferLength = dataArray.length;
    // }


    // 绘制波形图
    // x为水平坐标 递增
    // y为垂直坐标 递增
    // const [lines, setLines] = useState([
    //     { height: 10, x: 20 },
    //     { height: 30, x: 50 },
    //     { height: 20, x: 80 },
    //     { height: 40, x: 110 },
    //     { height: 25, x: 140 },
    //   ]);

    let i = 0

    const addLine = (v) => {
        // 根据具体的宽度动态调整声波图中两根竖线的距离
        i += 5
        const newLine = {
            // height: Math.floor(Math.random() * 100), // 随机高度在 10 到 60 之间
            height: v, // 随机高度在 10 到 60 之间
            x: i, // 随机 x 坐标
        };
        setLines(prev => [...prev, newLine]);
    };

    const onClose = () => {
        console.log('关闭, pcm: ', recorder.getPCMBlob());
        // 其他操作
        onDestroyRecord()
    }

    return (
        <div className={styles.container}>
            <div style={{ marginLeft: 40 }}>Speech测试</div>
            <div className={styles.outter}>
                <div className={styles.btns}>
                    <button onClick={onStartRecord}>开始录音</button>
                    <button onClick={onPauseRecord}>暂停录音</button>
                    <button onClick={onResumeRecord}>恢复录音</button>
                    <button onClick={onEndRecord}>结束录音</button>
                    <button onClick={onDestroyRecord}>断连</button>
                </div>
                <div className={styles.realtimeData}>
                    <div>录音时长(秒): {data.duration}</div>
                    <div>录音大小(kb): {data.fileSize / 1000}</div>
                    <div>录音音量百分比: : {data.vol} %</div>
                </div>
                <div className={styles.btns}>
                    <button onClick={onDownloadPCM}>下载PCM</button>
                    <button onClick={onDownloadWAV}>下载WAV</button>
                    <button onClick={logBase64}>Base64</button>
                    {/* <button onClick={onDownloadMP3}>下载mp3</button> */}
                </div>

                <div className={styles.waveContainer}>
                    <div className={styles.wave}>
                        <svg
                            width="100%"
                            height="100px"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <line
                                x1="0" // 起始点 x 坐标
                                y1="50" // 起始点 y 坐标（高度的一半）
                                x2="100%" // 结束点 x 坐标（自适应宽度）
                                y2="50" // 结束点 y 坐标（高度的一半）
                                stroke="#aaa" // 虚线颜色
                                strokeWidth="2" // 虚线宽度
                                strokeDasharray="5,5" // 虚线的样式
                            />
                            {/* 绘制竖线 */}
                            {lines.map((line, index) => (
                                <line
                                    key={index}
                                    x1={line.x}
                                    y1={50 - line.height / 2} // 固定底部位置
                                    x2={line.x}
                                    y2={50 + line.height / 2} // 根据高度计算 y2
                                    stroke="#111" // 竖线颜色
                                    strokeWidth="2" // 竖线宽度
                                />
                            ))}
                        </svg>
                    </div>
                    <div className={styles.countdown}>00:{timeDown}</div>
                    <Button className={styles.close} onClick={onClose}>关闭按钮</Button>
                </div>

                <SpeechBar node={{}}/>
            </div>
        </div>
    );
});
export default Speech;
