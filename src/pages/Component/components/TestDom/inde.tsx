import React, { FC, memo, useEffect } from 'react';
import './style.scss'

interface IndexProps { }

const TestDom: FC<IndexProps> = memo((props) => {
  const { } = props;
  useEffect(() => {
    const node = document.querySelector('.node main')
    console.log('node :>> ', node);
    const aa = node?.querySelector('.aaa')
    console.log('aa :>> ', aa);

    console.log(getComputedStyle(aa, 'after').backgroundColor);
    console.log(getComputedStyle(aa, 'after').width);
    console.log(getComputedStyle(aa, 'after').height);

    console.log('111',node.styleSheets);


  }, [])

  return (
    <div className='container'>
      <div className='node'>
        <header>
          你好 header
        </header>
        <main>
          main
          <div className='aaa'></div>
        </main>
        <footer>
          footer
        </footer>
      </div>
    </div>
  )
});
export default TestDom;