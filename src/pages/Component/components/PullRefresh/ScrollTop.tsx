import { FC, useCallback, useEffect, useRef, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
// import Loading from "../Loading";
// import "./index.css";
// import LoadingCat from "../LoadingCat";

interface ScrollProps {
  dataLength: number;
  fetchMoreData: () => void;
  children: React.ReactNode;
  style?: React.CSSProperties;
  hasMore?: boolean;
  stop?: number; // 回弹停留位置
  loadingTime?: number; // 加载时间
  pulldownRefresh: () => Promise<any>; // 下垃回调函数
  isPullDown?: boolean; // 第一次下拉刷新是否已经执行
}

const ScrollToTop: FC<ScrollProps> = ({
  dataLength,
  fetchMoreData,
  children,
  style,
  hasMore = true,
  stop = 50,
  loadingTime = 300,
  pulldownRefresh,
  isPullDown,
}) => {
  const viewRef = useRef<HTMLDivElement>(null);
  const [startY, setStartY] = useState(0);
  const [top, setTop] = useState(0);
  const [dynamic, setDynamic] = useState(false);

  const handleTouchStart = useCallback(
    (e) => {
      if (isPullDown) return;
      const [touches = {}] = e.changedTouches;
      const { pageY } = touches;

      setStartY(pageY);
    },
    [isPullDown],
  );

  const handleTouchMove = useCallback(
    (e) => {
      if (isPullDown) return;
      const [touches] = e.changedTouches;
      const { pageY } = touches;

      if (viewRef?.current) {
        const { scrollTop } = viewRef.current;
        const differ = pageY - startY;

        if (scrollTop === 0 && differ > 0) {
          const _diff = top > stop ? stop + Math.log10(differ) : top + 2;
          setTop(_diff);
        }
      }
    },
    [startY, top, stop, isPullDown],
  );

  const handleTouchEnd = useCallback(
    (e) => {
      if (isPullDown) return;
      const [touches] = e.changedTouches;
      const { pageY } = touches;

      if (viewRef?.current) {
        const { scrollTop } = viewRef.current;
        const differ = pageY - startY;

        if (scrollTop === 0 && differ > stop && differ > 20) {
          // 下拉超过阈值
          setTop(stop);
          setDynamic(true);
          pulldownRefresh()
            .then(() => {
              setTimeout(() => {
                setTop(0);
                setDynamic(false);
              }, loadingTime);
            })
            .catch(() => {
              setTimeout(() => {
                setTop(0);
                setDynamic(false);
              }, loadingTime);
            });
        } else {
          setTop(0);
        }
      }
    },
    [startY, stop, pulldownRefresh, loadingTime, isPullDown],
  );

  useEffect(() => {
    const view = viewRef?.current;

    if (view) {
      view.addEventListener('touchstart', handleTouchStart, { passive: false });
      view.addEventListener('touchmove', handleTouchMove, { passive: false });
      view.addEventListener('touchend', handleTouchEnd, { passive: false });
    }
    return () => {
      if (view) {
        view.removeEventListener('touchstart', handleTouchStart);
        view.removeEventListener('touchmove', handleTouchMove);
        view.removeEventListener('touchend', handleTouchEnd);
      }
    };
  }, [handleTouchStart, handleTouchMove, handleTouchEnd]);

  return (
    <div
      id="scrollableDiv"
      ref={viewRef}
      style={{
        height: 300,
        overflow: 'auto',
        display: 'flex',
        flexDirection: 'column-reverse',
        boxSizing: 'border-box',
        overflowAnchor: 'none',
        ...style,
      }}
    >
      {/* {!isPullDown && <LoadingCat dynamic={dynamic} />} */}
      {/*Put the scroll bar always on the bottom*/}
      <InfiniteScroll
        dataLength={dataLength}
        next={fetchMoreData}
        style={{
          display: 'flex',
          flexDirection: 'column-reverse',
          transform: `translate3d(0px, ${top}px, 0px)`,
        }} //To put endMessage and loader to the top.
        inverse={true}
        hasMore={hasMore}
        // loader={<Loading style={{ marginBottom: 10 }} />}
        loader={<div style={{ marginBottom: 10 }}>loading...</div>}
        hasChildren={true}
        scrollableTarget="scrollableDiv"
      >
        {children}
      </InfiniteScroll>
    </div>
  );
};

export default ScrollToTop;
