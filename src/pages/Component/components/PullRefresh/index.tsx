import React, { FC, memo } from 'react';

import styles from './style.module.scss';

const PullRefresh: FC = memo(() => {
  return (
    <div>
      <div>PullRefresh</div>
      <div id="box">
        <div className={styles.loaderBox}>
          <div id="loading"></div>
        </div>
        <h1>下拉刷新 ↓</h1>
      </div>
    </div>
  );
});
export default PullRefresh;
