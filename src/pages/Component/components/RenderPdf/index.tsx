import React, { FC, memo } from 'react';
import VConsole from 'vconsole';

import styles from './style.module.scss';

interface IndexProps {}
new VConsole();
const RenderPdf: FC<IndexProps> = memo((props) => {
  const getInfo = async () => {
    const data = {
      client_id: 'f4643963-a555-43b6-aa1d-0698c7a9580d',
      client_secret: '2e556c6a-ce0f-43a9-8ec0-9166d71a61ab',
    };
    const urll = new URLSearchParams();
    urll.append('params', JSON.stringify(data));
    try {
      const res = await fetch(
        'http://221.224.155.68:8000/epoint-smartcity-ssov2zs/rest/smartcitysso/getCurrentLoginUser',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: '*/*',
            Authorization: `Bearer ${'93222256cbfba8c62d631d26c595a966'}`,
          },
          body: urll.toString(),
        },
      );
      const data1 = await res.json();
      console.log('data1 :>> ', data1);
      const phonenum = res?.custom?.user?.mobile ?? '';
    } catch (e) {
      console.error(e);
    }
  };
  const fun = async () => {
    const data = {
      code: '04A1D2BEDB7697DDA1425F1F489C40960BB4B96E402F7333B37129E90886CE47A62C085649B6BED31F1AE26EBD5003EBC0676A4D6A4B5159995DD8E293FA263BAB8A4EB30B571C4EAB35CC9C8EAA7C47931E89C6458AF655DEEF6AD0823D75D4FB74704382F7825104ED25DB2970C0D04830E3545D3101635F94F6B8C29E2B31227A922BE5E1DC95579039F32849CBA61DFAB92D70AD9237C0497F7802DA0D4DFFB33C61D92909EDEB91CC812F1F93EF1A2DC40519',
      client_id: 'f4643963-a555-43b6-aa1d-0698c7a9580d',
      client_secret: '2e556c6a-ce0f-43a9-8ec0-9166d71a61ab',
    };
    const urll = new URLSearchParams();
    for (const [k, v] of Object.entries(data)) {
      urll.append(k, v);
    }

    console.log('urll :>> ', urll.toString());

    try {
      const res = await fetch(
        'http://221.224.155.68:8000/epoint-smartcity-ssov2zs/rest/common/getUserToken',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: '*/*',
          },
          body: urll.toString(),
        },
      );
      const data = await res.json();
      console.log('res :>> ', data);
      getInfo();
    } catch (e) {
      console.log('e :>> ', e);
    }
  };

  fun();
  return (
    <div className={styles.container}>
      <div className={styles.title}>render pdf</div>
      <div className={styles.inner}></div>
    </div>
  );
});
export default RenderPdf;
