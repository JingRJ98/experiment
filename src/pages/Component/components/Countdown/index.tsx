import React, { useEffect, useRef, useState } from 'react';
import { Progress } from 'antd';

import styles from './index.module.scss';

interface CountdownProps {
  initSeconds: number;
  onEndCountdown: () => void;
}

const Countdown = (props: CountdownProps) => {
  const { initSeconds, onEndCountdown } = props;

  const [seconds, setSeconds] = useState(initSeconds);

  const timeRef = useRef<NodeJS.Timeout | null>(null);

  useEffect(() => {
    timeRef.current = setInterval(() => {
      setSeconds((prevSeconds) => prevSeconds - 1);
    }, 1000);

    return () => {
      timeRef.current && clearInterval(timeRef.current);
    };
  }, []);

  useEffect(() => {
    if (seconds <= 0) {
      timeRef.current && clearInterval(timeRef.current);
      onEndCountdown?.();
    }
  }, [onEndCountdown, seconds]);

  return (
    <div className={styles.rtcCountdown}>
      <div className={styles.flipped}>
        <Progress
          type="circle"
          percent={(seconds / initSeconds) * 100}
          showInfo={false}
          strokeColor="#5a74f9"
          width={60}
        />
      </div>
        <div className={styles.seconds}>{seconds}</div>
      <div className={styles.content}>
        <div>检测到您已超过1分钟没有操作</div>
        <div>即将结束通话</div>
      </div>
    </div>
  );
};

export default Countdown;
