import React, { FC, memo, useRef, useState } from 'react';
import { Button } from 'antd';
import styles from './style.module.scss';

const Miaobiao: FC = memo(() => {
  const [timeArr, setTimeArr] = useState([]);
  const [start, setStart] = useState(0);
  const [isDisabled, setIsDisabled] = useState(true);
  const [pass, setPass] = useState(0);
  const timer = useRef(null);

  const format = (time) => {
    return time / 1000;
  };

  const onStart = () => {
    setIsDisabled(false);
    const now = Date.now();
    setStart(now);
    timer.current = setInterval(() => {
      setPass(Date.now() - now);
    }, 10);
  };

  const onClick = () => {
    setTimeArr((arr) => {
      const newArr = [...arr, format(Date.now() - start)];
      return newArr;
    });
  };

  const onInit = () => {
    setStart(0);
    setTimeArr([]);
    setIsDisabled(true);
    clearInterval(timer.current);
    setPass(0);
  };

  return (
    <div className={styles.container}>
      <p>秒表</p>
      <p>已经耗时: {pass / 1000}s</p>
      <div className={styles.inner}>
        {timeArr.map((v, i) => (
          <div key={i}>
            第{i + 1}个人用时: {v}s
          </div>
        ))}
      </div>
      <div className={styles.btns}>
        <Button onClick={onStart}>开始</Button>
        <Button onClick={onClick} disabled={isDisabled}>
          点击计时
        </Button>
        <Button onClick={onInit}>重置</Button>
      </div>
    </div>
  );
});
export default Miaobiao;
